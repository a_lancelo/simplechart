package com.gram.chart;

import java.util.ArrayList;

public class ChartFunction {
    ArrayList<Integer> points = new ArrayList<>();
    String columnName;
    String type;
    String name;
    String color;
    boolean isVisible;
    boolean needChangeVisibility;
    boolean isVisibleNewState;
    int alpha;
    float alphaSavedPercent;
}
