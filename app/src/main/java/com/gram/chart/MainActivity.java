package com.gram.chart;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends Activity {

    private int mThemeId = -1;
    boolean isDarkTheme;
    int width, height;
    String statusBarColor;
    String actionBarColor;
    String actionBarTextColor;
    String checkBoxTextColor;
    String checkBoxPanelColor;
    String checkBoxPanelDividedLineColor;
    String bottomPartColor;

    boolean isDesignV1;
    boolean isStandardTypeAnimation = true;
    MyScrollView scrollView;
    ArrayList<ChartView> chartViews = new ArrayList<>();
    ChartView chartView;
    //ViewGroup checkBoxPanel;
    RelativeLayout actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDisplaySize();
        setActionBar();
        setCustomTheme();
        setContentView(createFullView());
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.turn_theme) {
            turnTheme(item);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("theme", mThemeId);
    }

    public void turnTheme(MenuItem item) {
        isDarkTheme = !isDarkTheme;
        setCustomTheme();
    }

    void checkIsDesignV1() {
        int chartId = 0;
        if (getIntent().getExtras() != null) {
            chartId = getIntent().getExtras().getInt("id");
        }
        if (chartId == 0 || (chartId >= 2 && chartId < 8)) isDesignV1 = true;
    }

    void setColors() {
        statusBarColor = "#ffffff";
        actionBarColor = "#ffffff";
        actionBarTextColor = "#222222";
        checkBoxTextColor = "#222222";
        checkBoxPanelColor = "#ffffff";
        checkBoxPanelDividedLineColor = "#e0e0e0";
        bottomPartColor = "#f0f0f0";

        if (isDarkTheme) {
            statusBarColor = "#242f3e";
            actionBarColor = "#242f3e";
            actionBarTextColor = "#ffffff";
            checkBoxTextColor = "#ffffff";
            checkBoxPanelColor = "#1d2733";
            checkBoxPanelDividedLineColor = "#0b131e";
            bottomPartColor = "#151e27";
        }

        if (isDesignV1) {
            setColorsV1();
        }
    }

    void setColorsV1() {
        statusBarColor = "#426382";
        actionBarColor = "#517da2";
        actionBarTextColor = "#ffffff";

        if (isDarkTheme) {
            statusBarColor = "#1a242e";
            actionBarColor = "#212d3b";
        }
    }

    void setCustomTheme() {
        checkIsDesignV1();
        setColors();

        if (Build.VERSION.SDK_INT >= 21) {
            if (isDesignV1) {
                getWindow().setStatusBarColor(Color.parseColor(statusBarColor));
            } else {
                if (Build.VERSION.SDK_INT >= 23) {
                    getWindow().setStatusBarColor(Color.parseColor(statusBarColor));
                    getWindow().getDecorView().setSystemUiVisibility(isDarkTheme ? 0 : View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                } else if (Build.VERSION.SDK_INT >= 21) {
                    getWindow().setStatusBarColor(Color.parseColor("#242f3e"));
                }
            }
            getWindow().setNavigationBarColor(Color.parseColor(bottomPartColor));
        }

        if (actionBar != null) {
            actionBar.setBackgroundColor(Color.parseColor(actionBarColor));
            ((TextView)actionBar.getChildAt(0)).setTextColor(Color.parseColor(actionBarTextColor));
            if (!isDesignV1) {
                ((ImageView) actionBar.getChildAt(1)).setImageResource(isDarkTheme ? R.drawable.night_mode_5 : R.drawable.night_mode_4);
            }

            ((ViewGroup)scrollView.getParent()).getChildAt(1).setBackgroundResource(isDarkTheme ?
                    R.drawable.action_bar_dark_shadow : R.drawable.action_bar_light_shadow);
        }

        try {
            ActionBar actionBar = getActionBar();
            actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(actionBarColor)));
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowTitleEnabled(true);
        } catch (Exception e) {}

        if (scrollView != null) {
            scrollView.setBackgroundColor(Color.parseColor(bottomPartColor));
            if (((ViewGroup)scrollView.getChildAt(0)).getChildAt(0) instanceof ChartView) {
                ChartView chartView = (ChartView)((ViewGroup)scrollView.getChildAt(0)).getChildAt(0);
                updateThemeOnChartView(chartView);
            } else {
                ViewGroup mainLayout = (ViewGroup)scrollView.getChildAt(0);

                int count = (((ViewGroup)scrollView.getChildAt(0)).getChildCount());
                for (int i = 0; i < count; i++) {
                    if (mainLayout.getChildAt(i) instanceof ViewGroup
                            && ((ViewGroup) mainLayout.getChildAt(i)).getChildAt(0) instanceof ChartView) {
                        ChartView chartView = (ChartView)((ViewGroup)mainLayout.getChildAt(i)).getChildAt(0);
                        updateThemeOnChartView(chartView);
                    }
                }
            }
        }
    }

    void updateThemeOnChartView(ChartView chartView) {
        chartView.turnTheme(isDarkTheme);
        ((View)chartView.getParent()).setBackgroundColor(Color.parseColor(bottomPartColor));
        ((View)chartView.getParent().getParent()).setBackgroundColor(Color.parseColor(bottomPartColor));
        ((ViewGroup)chartView.getParent()).getChildAt(((ViewGroup)chartView.getParent()).getChildCount() - 1)
                .setBackgroundColor(Color.parseColor(checkBoxPanelColor));

        int childCount = ((ViewGroup)chartView.getParent()).getChildCount();
        if (childCount == 2) {
            ViewGroup checkBoxPanel = (ViewGroup) (((ViewGroup) chartView.getParent()).getChildAt(childCount - 1));
            for (int i = 0; i < checkBoxPanel.getChildCount(); i++) {
                if (checkBoxPanel.getChildAt(i) instanceof CheckBox) {
                    ((CheckBox) checkBoxPanel.getChildAt(i)).setTextColor(Color.parseColor(checkBoxTextColor));
                } else if (checkBoxPanel.getChildAt(i) instanceof ViewGroup) {
                    ViewGroup group = (ViewGroup) checkBoxPanel.getChildAt(i);
                    for (int j = 0; j < group.getChildCount(); j++) {
                        if (group.getChildAt(j) instanceof CheckBox) {
                            ((CheckBox) group.getChildAt(j)).setTextColor(Color.parseColor(checkBoxTextColor));
                        }
                    }
                } else if (checkBoxPanel.getChildAt(i).getTag().equals("lineView")) {
                    (checkBoxPanel.getChildAt(i)).setBackgroundColor(Color.parseColor(checkBoxPanelDividedLineColor));
                }
            }
        }
    }

    void setActionBar() {
        setTitle(R.string.statistics);
        try {
            getActionBar().hide();
        } catch (Exception e) {}

        if (true) return;

        if (height < dp(500)) {
            try {
                getActionBar().hide();
            } catch (Exception e) {}
        }
    }

    View createActionBar() {
        actionBar = new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int)dp(56));
        actionBar.setLayoutParams(layoutParams);
        actionBar.setBackgroundColor(Color.parseColor(actionBarColor));

        TextView title = new TextView(this);
        title.setTextColor(Color.parseColor(actionBarTextColor));
        title.setTypeface(Typeface.DEFAULT, 1);

        title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        title.setText(R.string.statistics);
        title.setGravity(Gravity.CENTER_VERTICAL);

        RelativeLayout.LayoutParams titleLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        titleLayoutParams.setMargins((int)dp(20), 0, 0, 0);
        //titleLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        title.setLayoutParams(titleLayoutParams);
        actionBar.addView(title);

        ImageView imageView = new ImageView(this);
        RelativeLayout.LayoutParams imageLayoutParams = new RelativeLayout.LayoutParams((int)dp(56), (int)dp(56));
        imageLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        imageView.setLayoutParams(imageLayoutParams);
        int imagePadding = (int)dp(10);
        imageView.setPadding(imagePadding, imagePadding, imagePadding, imagePadding);
        imageView.setImageResource(isDesignV1 ? R.drawable.night_mode_3 : R.drawable.night_mode_4);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                turnTheme(null);
            }
        });
        actionBar.addView(imageView);

        return actionBar;
    }

    View createFullView() {
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        layout.addView(createActionBar());
        layout.addView(createViewWithActionBarShadow());
        return layout;
    }

    View createViewWithActionBarShadow() {
        FrameLayout frameLayout = new FrameLayout(this);
        frameLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        frameLayout.addView(createView());

        View actionBarShadow = new View(this);
        actionBarShadow.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int)dp(2.5f)));
        actionBarShadow.setBackgroundResource(R.drawable.action_bar_light_shadow);
        frameLayout.addView(actionBarShadow);
        return frameLayout;
    }

    View createView() {
        int chartId = 0;
        if (getIntent().getExtras() != null) {
            chartId = getIntent().getExtras().getInt("id");
        }

        scrollView = new MyScrollView(this);
        scrollView.setVerticalScrollBarEnabled(false);
        scrollView.setBackgroundColor(Color.parseColor(bottomPartColor));

        if (chartId >= 2 && chartId <= 12) {
            scrollView.addView(createFullChartView(chartId - 2));
        } else if (chartId == 0 || chartId == 1) {
            int beginChartPos = chartId == 0 ? 0 : 6;
            int endChartPos = chartId == 0 ? 5 : 11;

            LinearLayout mainLayout = new LinearLayout(this);
            mainLayout.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int)dp(100));

            for (int i = beginChartPos; i < endChartPos; i++) {
                View fullChartView = createFullChartView(i);
                ((LinearLayout.LayoutParams) fullChartView.getLayoutParams()).
                        setMargins(0, (int)dp(i == beginChartPos ? 30 : 0), 0, (int) dp(i + 1 == endChartPos ? 30 : 100));
                mainLayout.addView(fullChartView);
            }
            scrollView.addView(mainLayout);
        }
        return scrollView;
    }

    View createFullChartView(int chartId) {
        if (getIntent().getExtras() != null) {
            isStandardTypeAnimation = getIntent().getExtras().getBoolean("isStandardTypeAnimation");
        }

        LinearLayout mainLayout = new LinearLayout(this);
        LinearLayout.LayoutParams mainLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mainLayout.setOrientation(LinearLayout.VERTICAL);
        mainLayout.setLayoutParams(mainLayoutParams);
        mainLayout.setBackgroundColor(Color.parseColor(bottomPartColor));

        ChartData data = AppData.allTestData.get(chartId);
        int heightChartView = (int)dp(440);

        String chartTitle = "Chart #"+ String.valueOf(chartId - (chartId >= 6 ? 6 : 0) + 1);
        chartView = new ChartView(this, width, height, chartTitle, isDarkTheme, isStandardTypeAnimation, data);
        heightChartView += chartView.getCustomCheckboxPanelHeight();
        LinearLayout.LayoutParams chartLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, heightChartView);//(int)dp(420));
        mainLayout.addView(chartView, chartLayoutParams);
        if (data.isDesignV1) {
            mainLayout.addView(createCheckBoxPanel(chartView));
        }
        chartViews.add(chartView);
        return mainLayout;
    }

    CheckBox addCheckBoxToView(LinearLayout view, final ChartView chartView, final int i) {
        int checkBoxStates[][] = {{android.R.attr.state_checked}, {}};
        CheckBox checkBox1 = new CheckBox(this);
        checkBox1.setText(chartView.data.functions.get(i).name);
        checkBox1.setTextColor(Color.parseColor(checkBoxTextColor));
        checkBox1.setTextSize(16);
        checkBox1.setPadding((int)dp(12), 0, 0, 0);
        checkBox1.setChecked(true);
        checkBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                chartView.changeChartState(i, b);
            }
        });
        if (Build.VERSION.SDK_INT >= 21) {
            checkBox1.setButtonTintList(new ColorStateList(checkBoxStates, new int[]{Color.parseColor(chartView.data.functions.get(i).color), Color.GRAY}));
        }
        return checkBox1;
    }

    View createCheckBoxPanel(ChartView chartView) {
        int countCheckBoxes = chartView.data.functions.size();

        LinearLayout checkBoxPanelLayout = new LinearLayout(this);
        checkBoxPanelLayout.setOrientation(LinearLayout.VERTICAL);
        checkBoxPanelLayout.setBackgroundColor(Color.parseColor(checkBoxPanelColor));//"#ffffff"));

        LinearLayout.LayoutParams checkBoxLayoutParams2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        checkBoxLayoutParams2.weight = 1;

        LinearLayout checkBoxPanel1 = new LinearLayout(this);
        boolean isSupportTwoInRow = countCheckBoxes > 2;

        for (int i = 0; i < countCheckBoxes; i++) {
            if (i % 2 == 0) {
                checkBoxPanel1 = new LinearLayout(this);
                checkBoxPanel1.setOrientation(LinearLayout.HORIZONTAL);
                checkBoxPanelLayout.addView(checkBoxPanel1);
            }

            CheckBox checkBox = addCheckBoxToView(checkBoxPanelLayout, chartView, i);
            LinearLayout.LayoutParams checkBoxLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            checkBoxLayoutParams.weight = 1;

            if (isSupportTwoInRow) {
                checkBoxPanel1.addView(checkBox, checkBoxLayoutParams);
            } else {
                checkBoxPanelLayout.addView(checkBox, checkBoxLayoutParams);
            }

            int topMargin = (int) dp(isSupportTwoInRow ? (i < 2 ? 0 : 10) : (i < 1 ? 0 : 10));

            ((LinearLayout.LayoutParams) checkBox.getLayoutParams()).setMargins((int) dp(20), topMargin, 0, (int) dp(10));

            View lineView = new View(this);
            lineView.setTag("lineView");
            lineView.setBackgroundColor(Color.parseColor(checkBoxPanelDividedLineColor));//"#e0e0e0"));//"#f0f0f0"));
            LinearLayout.LayoutParams lineViewParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) dp(0.5f));
            lineViewParams.setMargins((int) dp(62), 0, 0, 0);
            lineView.setLayoutParams(lineViewParams);
            checkBoxPanelLayout.addView(lineView);
        }
        return checkBoxPanelLayout;
    }

    void getDisplaySize() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
        Log.d("S", "size x = "+width+"  size y = "+height);
    }

    float dp(float px) {
        Resources r = getResources();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, px, r.getDisplayMetrics());
    }

    private class MyScrollView extends ScrollView {

        public MyScrollView(Context context) {
            super(context);
        }

        private boolean mScrollable = true;

        public void setScrollingEnabled(boolean enabled) {
            mScrollable = enabled;
        }

        public boolean isScrollable() {
            boolean isScrollEnabled = true;
            for (int i = 0; i < chartViews.size(); i++) {
                if (!chartViews.get(i).isScrollEnabled()) {
                    isScrollEnabled = false;
                    break;
                }
            }
            return isScrollEnabled;
        }

        @Override
        public boolean onTouchEvent(MotionEvent ev) {
            switch (ev.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    return isScrollable() && super.onTouchEvent(ev);
                default:
                    return super.onTouchEvent(ev);
            }
        }

        @Override
        public boolean onInterceptTouchEvent(MotionEvent ev) {
            return isScrollable() && super.onInterceptTouchEvent(ev);
        }

    }
}
