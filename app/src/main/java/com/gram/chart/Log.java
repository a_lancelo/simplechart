package com.gram.chart;

/**
 * Created by Andrew on 12.04.2019.
 */

public class Log {

    //static boolean isLogEnabled = true;
    static boolean isLogEnabled = false;

    public static void d(String tag, String msg) {
        if (!isLogEnabled) return;
        android.util.Log.d(tag, msg);
    }
}
