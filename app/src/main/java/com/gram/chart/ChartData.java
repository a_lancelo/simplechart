package com.gram.chart;

import java.util.ArrayList;

public class ChartData {
    ArrayList<Long> x = new ArrayList<>();
    ArrayList<String> dates = new ArrayList<>();
    ArrayList<String> datesWithNameOfDay = new ArrayList<>();
    ArrayList<String> datesWithNameOfDayAndYear = new ArrayList<>();
    ArrayList<String> datesWithYear = new ArrayList<>();
    ArrayList<ChartFunction> functions = new ArrayList<>();
    int countElements;
    int type;
    boolean is2YAxis;
    boolean isDesignV1;
    //ChartData zoomedData;
    boolean isSupportZoom;
    ArrayList<ChartData> allDataZoomed = new ArrayList<>();
    ChartData dataZoomed;
}
