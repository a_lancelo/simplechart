package com.gram.chart;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Handler;

import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import java.util.ArrayList;
import java.util.Locale;

public class ChartView extends View {
    
    ArrayList<Integer> lineValues = new ArrayList<>();
    ArrayList<Integer> lineValuesLast = new ArrayList<>();

    ArrayList<Integer> lineValuesRight = new ArrayList<>();
    ArrayList<Integer> lineValuesRightLast = new ArrayList<>();
    
    ChartData data;
    
    float bPos, ePos;
    String LOG_TAG = "ChartView";
    Paint p1, p2, p3, p4, p5, p6, p18;
    ArrayList<Paint> functionPaints = new ArrayList<>();
    ArrayList<Paint> functionPaintsForScrollPanel = new ArrayList<>();
    int width, height;
    int marginsX;

    int beginPos;
    int chartMaxHeight;

    int scrollAreaWidth, scrollAreaHeight, scrollAreaX, scrollAreaY, minScrollAreaWidth;
    boolean scrollAreaIsCaptured;
    
    boolean scrollAreaChangeModeLeft, scrollAreaChangeModeRight;
    float lastTouchX, lastTouchY;
    
    boolean isChoseValueOnChartMode;

    boolean changeChartAnimationIsGrow;

    boolean changeVisibilityAnimate;
    boolean changeVisibilityAnimateNeedRebuildChartLines;
    int changeVisibilityAnimationDuration;
    int changeVisibilityAnimationTime = 0;

    boolean changeMotionYAnimate;
    int changeMotionYAnimationDuration;
    int changeMotionYAnimationTime = 0;

    int scrollPanelAnimationTime = 0;
    int scrollPanelAnimationMaxValueBegin;
    int scrollPanelAnimationMinValueBegin;
    int scrollPanelAnimationMaxValueEnd;
    int scrollPanelAnimationMinValueEnd;

    boolean valueWasChosenOnChart;
    int chosenValueOnChart;
    float chosenValueOnChartXPos;
    float chosenValueOnChartYMinPos;

    String chartTitle;
    boolean isDarkTheme;
    boolean isStandardTypeAnimation;

    String backgroundColor;
    String chartTitleTextColor;
    String chartYAxisValueColor;
    String chartXAxisValueColor;
    String chartLineColor;
    String rectChosenValueBackgroundColor;
    String rectChosenValueLineColor;
    String rectChosenValueDateColor;
    String rectChosenValueBorder1Color;
    String rectChosenValueBorder2Color;
    String scrollPanelColor;
    String scrollPanelBordersColor;

    ArrayList<Rect> customCheckboxesRect = new ArrayList<>();
    ArrayList<Boolean> customCheckboxesStates = new ArrayList<>();

    boolean isChartWith2YAxis;

    ScaleGestureDetector scaleGestureDetector;

    boolean isZoomEnabled = false;
    boolean isZoomed;
    float bPosZoom, ePosZoom;

    public ChartView(Context context) {
        
        this(context, 0, 0);
    }
    
    public ChartView(Context context, int width, int height) {
        this(context, width, height, "", false, true, null);
    }

    public ChartView(Context context, int width, int height, String chartTitle, boolean isDarkTheme, boolean isStandardTypeAnimation, ChartData data) {
        super(context);

        this.width = width;
        this.height = height;

        this.chartTitle = chartTitle;
        this.isDarkTheme = isDarkTheme;
        this.isStandardTypeAnimation = isStandardTypeAnimation;
        this.data = data;

        marginsX = (int)dp(15);

        beginPos = (int)dp(320);
        chartMaxHeight = (int)dp(300);

        scrollAreaWidth = (width - (marginsX * 2)) / 4;
        scrollAreaHeight = (int)dp(50);

        minScrollAreaWidth = (int)((width - (marginsX * 2)) / (float) data.countElements * 6);

        if (scrollAreaWidth < minScrollAreaWidth) scrollAreaWidth = minScrollAreaWidth;

        Log.d("minScrollAreaWidth", ""+minScrollAreaWidth);

        scrollAreaX = marginsX;
        scrollAreaY = beginPos + (int)dp(50);

        Log.d(LOG_TAG, "func size = "+data.functions.size()+" "+data.functions.get(0).points.size());
        setColors();
        setPaints();

        updateBeginAndEndPos();
        generateLineValues();
        testMaxValueForBuildChart();

        scaleGestureDetector = new ScaleGestureDetector(context, new ScaleListener());
    }

    public void changeChartState(int chartId, boolean enabled) {

        valueWasChosenOnChart = false;

        boolean restartAnimation = false;

        if (changeVisibilityAnimate || changeMotionYAnimate) {
            restartAnimation = true;
        }

        data.functions.get(chartId).isVisible = enabled;
        data.functions.get(chartId).needChangeVisibility = true;
        data.functions.get(chartId).isVisibleNewState = enabled;

        recountLineValues(restartAnimation);

        if (restartAnimation) {
            hAnimVisibility.removeMessages(0);
            hAnimMotionY.removeMessages(0);
        }

        int minValueLast = lineValuesLast.get(0);
        int maxValueLast = lineValuesLast.get(lineValues.size() - 1);

        int minValue = lineValues.get(0);
        int maxValue = lineValues.get(lineValues.size() - 1);

        changeVisibilityAnimateNeedRebuildChartLines = (maxValue != maxValueLast || minValue != minValueLast);
        changeChartAnimationIsGrow = lineValuesLast.get(lineValuesLast.size() - 1) < lineValues.get(lineValues.size() - 1);

        float diff1 = maxValue - minValue;
        float diff2 = maxValueLast - minValueLast;
        if (diff2 > diff1) {
            diff1 = diff2;
            diff2 = maxValue - minValue;
        }
        float diff = diff1 / diff2;

        changeVisibilityAnimationDuration = (int)(diff * 500);
        if (changeVisibilityAnimationDuration < 250) {
            changeVisibilityAnimationDuration = 250;
        }
        if (changeVisibilityAnimationDuration > 2500) {
            changeVisibilityAnimationDuration = 2500;
        }

        changeVisibilityAnimationDuration = 1000;

        changeVisibilityAnimationDuration = calcAnimationDuration();

        startAnimationChangeVisibility();
    }

    int calcAnimationDuration() {
        return 200;
    }

    void recountLineValuesForZoom() {
        int maxValue = getMaxValueForBuildChart(getMaxValueByVisibleFunctionsByPart(true));
        
        int minValue = getMinValueForBuildChart(getMinValueByVisibleFunctionsByPart(true), maxValue);

        Log.d("recountLineValuesForZoom", "min = "+minValue+"   max = "+maxValue+"   isZoomed = "+isZoomed);

        lineValuesLast.clear();
        for (int i = 0; i < lineValues.size(); i++) {
            lineValuesLast.add(lineValues.get(i));
        }

        lineValues.clear();

        for (int i = 0; i < 6; i++) {
            lineValues.add(minValue + ((maxValue - minValue) * i / 5));
        }

        Log.d("recountLineValuesForZoom2", "min = "+lineValues.get(0)+"   max = "+lineValues.get(5)+"   isZoomed = "+isZoomed);

        invalidate();
    }

    void recountLineValues() {
        recountLineValues(false);
    }

    void recountLineValues(boolean forRestartAnimation) {
        if (isZoomed) {
            recountLineValuesForZoom();
            return;
        }

        if (forRestartAnimation) {
            forRestartAnimation = changeMotionYAnimationTime <= (changeMotionYAnimationDuration / 2);
        }

        forRestartAnimation = false;


        int maxValueOrig = getMaxValueByVisibleFunctionsByPart();
        int minValueOrig = getMinValueByVisibleFunctionsByPart();


        if (data.is2YAxis) {
            maxValueOrig = getMaxValueByVisibleFunctionsByPart(0, 1);
            minValueOrig = getMinValueByVisibleFunctionsByPart(0, 1);
        }

        int maxValue = getMaxValueForBuildChart(maxValueOrig, minValueOrig);
        int minValue = getMinValueForBuildChart(minValueOrig, maxValue);

        if (!forRestartAnimation) {
            lineValuesLast.clear();
            for (int i = 0; i < lineValues.size(); i++) {
                lineValuesLast.add(lineValues.get(i));
            }
        } else {
            
        }

        lineValues.clear();

        for (int i = 0; i < 6; i++) {
            lineValues.add(minValue + ((maxValue - minValue) * i / 5));
        }

        if (data.is2YAxis) {
            maxValueOrig = getMaxValueByVisibleFunctionsByPart(1, 0);
            minValueOrig = getMinValueByVisibleFunctionsByPart(1, 0);

            maxValue = getMaxValueForBuildChart(maxValueOrig, minValueOrig);
            minValue = getMinValueForBuildChart(minValueOrig, maxValue);

            if (!forRestartAnimation) {
                lineValuesRightLast.clear();
                for (int i = 0; i < lineValuesRight.size(); i++) {
                    lineValuesRightLast.add(lineValuesRight.get(i));
                }
            } else {
                
            }

            lineValuesRight.clear();
            for (int i = 0; i < 6; i++) {
                lineValuesRight.add(minValue + ((maxValue - minValue) * i / 5));
            }
        }

        invalidate();
    }

    Point getIntermediateMaxMinValues() {
        return getIntermediateMaxMinValues(false);
    }

    Point getIntermediateMaxMinValues(boolean is2AxisRightMode) {
        return getIntermediateMaxMinValues(is2AxisRightMode ? lineValuesRight : lineValues,
                is2AxisRightMode ? lineValuesRightLast : lineValuesLast);
    }

    Point getIntermediateMaxMinValues(ArrayList<Integer> lineValues, ArrayList<Integer> lineValuesLast) {
        int sizeBetweenLines = lineValues.get(lineValues.size() - 1) - lineValues.get(lineValues.size() - 2);
        int maxValue = lineValues.get(lineValues.size() - 1) + sizeBetweenLines;
        int minValue = lineValues.get(0);

        if (lineValuesLast.size() == 0) {
            return new Point(maxValue, minValue);
        }

        sizeBetweenLines = lineValuesLast.get(lineValuesLast.size() - 1) - lineValuesLast.get(lineValuesLast.size() - 2);
        int maxValue1 = lineValuesLast.get(lineValuesLast.size() - 1) + sizeBetweenLines;
        int minValue1 = lineValuesLast.get(0);

        if (maxValue1 == maxValue && minValue1 == minValue) {
            return new Point(maxValue, minValue);
        }

        int time = 0;
        int duration = 1;

        if (changeVisibilityAnimate) {
            time = changeVisibilityAnimationTime;
            duration = changeVisibilityAnimationDuration;
        }

        if (changeMotionYAnimate) {
            time = changeMotionYAnimationTime;
            duration = changeMotionYAnimationDuration;
        }

        float percentOfAnimation = (float)time / duration;
        
        if (maxValue == 5) {
            
        }

        int point1 = data.functions.get(0).points.get(0);
        int point2 = data.functions.get(0).points.get(1);

        float rel1, rel1Last;
        float rel2, rel2Last;

        rel1 = (point1 - minValue) / (float)(maxValue - minValue);
        rel1Last = (point1 - minValue1) / (float)(maxValue1 - minValue1);

        rel2 = (point2 - minValue) / (float)(maxValue - minValue);
        rel2Last = (point2 - minValue1) / (float)(maxValue1 - minValue1);

        if (maxValue == 5) {
            
        }

        if (maxValue1 == 5 || maxValue1 == 6 || maxValue1 == 8 || maxValue1 == 9) {

        }

        rel1 = (rel1 + ((rel1Last - rel1) * (1 - percentOfAnimation)));
        rel2 = (rel2 + ((rel2Last - rel2) * (1 - percentOfAnimation)));

        Log.d("zkkf", "minV1 = "+minValue1+"  maxV1 = "+maxValue1+"  minV = "+minValue+"  maxV = "+maxValue);

        float rel3 = rel1 / rel2;

        minValue = (int)((rel3 * point2 - point1) / (rel3 - 1));

        float heightFromMinToMax = (point1 - minValue) / rel1;
        float heightFromMinToMax2 = (point2 - minValue) / rel2;

        maxValue = (int)(minValue + heightFromMinToMax);

        minValue1 = minValue;
        maxValue1 = maxValue;
        
        Log.d("zkkk", "minV = "+minValue+"  maxV = "+maxValue+"  hMnTMx1 = "+heightFromMinToMax+"  hMnTMx2 = "+heightFromMinToMax2+"  p/min = "+(point1 - minValue)+"  rel1 = "+rel1);

        return new Point(maxValue1, minValue1);
    }

    void testMaxValueForBuildChart() {
        getMinValueForBuildChart(17, 108);
        getMinValueForBuildChart(18, 108);
        getMinValueForBuildChart(20, 108);
        getMinValueForBuildChart(0, 6);
        getMinValueForBuildChart(1, 6);
        getMinValueForBuildChart(2, 6);
        getMinValueForBuildChart(3, 6);
        getMinValueForBuildChart(4, 6);
        getMinValueForBuildChart(5, 6);
        getMinValueForBuildChart(6, 6);
    }

    int getMaxValueForBuildChart(int maxValue) {
        int minVal = getMinValueByVisibleFunctionsByPart();
        int val = maxValue - ((maxValue - minVal) / (5 * 2));
        boolean needAdd5 = val % 10 > 5;
        val = val - (val % 10) + (needAdd5 ? 5 : 0);
        if (val <= 30) {
            boolean needAddMax5 = (maxValue % 10) >= 5;
            val = maxValue + ((needAddMax5 ? 10 : 5) - (maxValue % 10));
        }
        return val;
    }

    int getMaxValueForBuildChart(int maxValue, int minVal) {
        int val = maxValue - ((maxValue - minVal) / (5 * 2));

        boolean needAdd5 = val % 10 > 5;
        val = val - (val % 10) + (needAdd5 ? 5 : 0);

        if (val <= 30) {
            boolean needAddMax5 = (maxValue % 10) >= 5;
            val = maxValue + ((needAddMax5 ? 10 : 5) - (maxValue % 10));
        }
        return val;
    }

    int getMinValueForBuildChart(int minValue, int maxValue) {
        if (((float)minValue / maxValue) < 0.17) {
            return 0;
        }

        int minValueF = minValue - ((maxValue - minValue) / 10);
        int minValueF2 = minValueF - (minValueF % 5);
        if (minValueF2 >= 0 && minValueF2 < minValueF) minValueF = minValueF2;
        return minValueF;
    }

    int getMaxValueByVisibleFunctions() {
        return getMaxValueByVisibleFunctions(false, -1, -1);
    }

    int getMaxValueByVisibleFunctions(boolean isZoomed) {
        return getMaxValueByVisibleFunctions(isZoomed, -1, -1);
    }

    int getMaxValueByVisibleFunctions(int withChartId, int withoutChartId) {
        return getMaxValueByVisibleFunctions(false, withChartId, withoutChartId);
    }

    int getMaxValueByVisibleFunctions(boolean isZoomed, int withChartId, int withoutChartId) {
        if (data.type == CHART_TYPE_STACK) {
            return getMaxValueByVisibleFunctionsType2(isZoomed, withChartId, withoutChartId);
        }
        return getMaxValueByVisibleFunctionsType0(isZoomed, withChartId, withoutChartId);
    }

    int getMaxValueByVisibleFunctionsType0(boolean isZoomed, int withChartId, int withoutChartId) {
        ChartData data = isZoomed ? this.data.dataZoomed : this.data;
        float bPos = isZoomed ? this.bPosZoom : this.bPos;
        float ePos = isZoomed ? this.ePosZoom : this.ePos;

        int maxValue = 0;
        for (int i = 0; i < data.functions.size(); i++) {
            if (i == withoutChartId) continue;
            ChartFunction func = data.functions.get(i);
            if (i != withChartId && !func.isVisible) continue;

            for (int j = 0; j < func.points.size(); j++) {
                if (func.points.get(j) > maxValue) maxValue = func.points.get(j);
            }
        }
        return maxValue;
    }

    int getMaxValueByVisibleFunctionsType2(boolean isZoomed, int withChartId, int withoutChartId) {
        ChartData data = isZoomed ? this.data.dataZoomed : this.data;
        float bPos = isZoomed ? this.bPosZoom : this.bPos;
        float ePos = isZoomed ? this.ePosZoom : this.ePos;

        int maxValue = 0;

        for (int j = 0; j < data.countElements; j++) {
            int sum = 0;

            for (int i = 0; i < data.functions.size(); i++) {
                if (i == withoutChartId) continue;
                ChartFunction func = data.functions.get(i);
                if (i != withChartId && !func.isVisible) continue;

                sum += func.points.get(j);
            }

            if (sum > maxValue) maxValue = sum;
        }
        return maxValue;
    }

    int getMaxValueByVisibleFunctionsWith(int chartId) {
        return getMaxValueByVisibleFunctions(chartId, -1);
    }

    int getMaxValueByVisibleFunctionsWithout(int chartId) {
        return getMaxValueByVisibleFunctions(-1, chartId);
    }


    int getMaxValueByVisibleFunctionsByPart() {
        return getMaxValueByVisibleFunctionsByPart(-1, -1);
    }

    int getMaxValueByVisibleFunctionsByPart(boolean isZoomed) {
        return getMaxValueByVisibleFunctionsByPart(isZoomed, -1, -1);
    }

    int getMaxValueByVisibleFunctionsByPart(int withChartId, int withoutChartId) {
        return getMaxValueByVisibleFunctionsByPart(false, withChartId, withoutChartId);
    }

    int getMaxValueByVisibleFunctionsByPart(boolean isZoomed, int withChartId, int withoutChartId) {
        if (data.type == CHART_TYPE_STACK) {
            return getMaxValueByVisibleFunctionsByPartType2(isZoomed, withChartId, withoutChartId);
        }
        return getMaxValueByVisibleFunctionsByPartType0(isZoomed, withChartId, withoutChartId);
    }

    int getMaxValueByVisibleFunctionsByPartType0(int withChartId, int withoutChartId) {
        return getMaxValueByVisibleFunctionsByPartType0(false, withChartId, withoutChartId);
    }

    int getMaxValueByVisibleFunctionsByPartType0(boolean isZoomed, int withChartId, int withoutChartId) {
        ChartData data = isZoomed ? this.data.dataZoomed : this.data;
        float bPos = isZoomed ? this.bPosZoom : this.bPos;
        float ePos = isZoomed ? this.ePosZoom : this.ePos;

        int maxValue = 0;
        for (int i = 0; i < data.functions.size(); i++) {
            if (i == withoutChartId) continue;
            ChartFunction func = data.functions.get(i);
            if (i != withChartId && !func.isVisible) continue;

            for (int j = (int) bPos; j < (int) ePos; j++) {
                if (func.points.get(j) > maxValue) maxValue = func.points.get(j);
            }
        }
        return maxValue;
    }

    int getMaxValueByVisibleFunctionsByPartType2(int withChartId, int withoutChartId) {
        return getMaxValueByVisibleFunctionsByPartType2(false, withChartId, withoutChartId);
    }

    int getMaxValueByVisibleFunctionsByPartType2(boolean isZoomed, int withChartId, int withoutChartId) {
        ChartData data = isZoomed ? this.data.dataZoomed : this.data;
        float bPos = isZoomed ? this.bPosZoom : this.bPos;
        float ePos = isZoomed ? this.ePosZoom : this.ePos;

        int maxValue = 0;

        for (int j = (int) bPos; j < (int) ePos; j++) {
            int sum = 0;

            for (int i = 0; i < data.functions.size(); i++) {
                if (i == withoutChartId) continue;
                ChartFunction func = data.functions.get(i);
                if (i != withChartId && !func.isVisible) continue;

                sum += func.points.get(j);
            }

            if (sum > maxValue) maxValue = sum;
        }
        return maxValue;
    }

    int getMinValueByVisibleFunctionsByPart() {
        return getMinValueByVisibleFunctionsByPart(-1, -1);
    }

    int getMinValueByVisibleFunctionsByPart(boolean isZoomed) {
        return getMinValueByVisibleFunctionsByPart(isZoomed, -1, -1);
    }

    int getMinValueByVisibleFunctionsByPart(int withChartId, int withoutChartId) {
        return getMinValueByVisibleFunctionsByPart(false, withChartId, withoutChartId);
    }

    int getMinValueByVisibleFunctionsByPart(boolean isZoomed, int withChartId, int withoutChartId) {
        if (data.type == CHART_TYPE_STACK) {
            return 0;
        }

        ChartData data = isZoomed ? this.data.dataZoomed : this.data;
        float bPos = isZoomed ? this.bPosZoom : this.bPos;
        float ePos = isZoomed ? this.ePosZoom : this.ePos;

        boolean firstMinValueWasSet = false;
        int minValue = 0;
        for (int i = 0; i < data.functions.size(); i++) {
            if (i == withoutChartId) continue;
            ChartFunction func = data.functions.get(i);
            if (i != withChartId && !func.isVisible) continue;

            for (int j = (int) bPos; j < (int) ePos; j++) {
                if (!firstMinValueWasSet) {
                    firstMinValueWasSet = true;
                    minValue = func.points.get(j);
                }
                if (func.points.get(j) < minValue) minValue = func.points.get(j);
            }
        }
        return minValue;
    }

    int getMinValueByVisibleFunctions() {
        return getMinValueByVisibleFunctions(-1, -1);
    }

    int getMinValueByVisibleFunctions(int withChartId, int withoutChartId) {
        boolean firstMinValueWasSet = false;
        int minValue = 0;
        for (int i = 0; i < data.functions.size(); i++) {
            if (i == withoutChartId) continue;
            ChartFunction func = data.functions.get(i);
            if (i != withChartId && !func.isVisible) continue;

            for (int j = 0; j < func.points.size(); j++) {
                if (!firstMinValueWasSet) {
                    firstMinValueWasSet = true;
                    minValue = func.points.get(j);
                }
                if (func.points.get(j) < minValue) minValue = func.points.get(j);
            }
        }
        return minValue;
    }

    Handler hAnimVisibility = new Handler() {
        public void handleMessage(android.os.Message msg) {
            animateChangeVisibility();
        };
    };

    Handler hAnimMotionY = new Handler() {
        public void handleMessage(android.os.Message msg) {
            animateMotionY();
        };
    };

    void startAnimationChangeVisibility() {
        startTimeAnimationLog = System.currentTimeMillis();
        Log.d("animationStart", "changeVisibility");
        changeVisibilityAnimate = true;
        changeMotionYAnimate = false;
        changeVisibilityAnimationTime = 0;
        scrollPanelAnimationTime = 0;
        animateChangeVisibility();
    }

    long lastTime = 0;

    void animateChangeVisibility() {
        
        invalidate();

        long diff = System.currentTimeMillis() - lastTime;
        lastTime = System.currentTimeMillis();
        if (diff > 60) diff = 60;

        changeVisibilityAnimationTime += diff;
        scrollPanelAnimationTime += diff;
        
        if (changeVisibilityAnimationTime < changeVisibilityAnimationDuration) {
            hAnimVisibility.sendEmptyMessageDelayed(0, 50);
        } else {
            endAnimationChangeVisibility();
        }
    }

    void endAnimationChangeVisibility() {
        long time = System.currentTimeMillis() - startTimeAnimationLog;
        Log.d("animationEnd", "changeVisibility = "+time);
        changeVisibilityAnimate = false;
        changeVisibilityAnimationTime = 0;
        scrollPanelAnimationTime = 0;
        changeVisibilityStates();
    }

    void changeVisibilityStates() {
        for (int i = 0; i < data.functions.size(); i++) {
            if (data.functions.get(i).needChangeVisibility) {
                data.functions.get(i).needChangeVisibility = false;
                data.functions.get(i).isVisible = data.functions.get(i).isVisibleNewState;
                data.functions.get(i).alphaSavedPercent = 0;
            }
        }
    }

    Handler hCheckAnimateMotionYIfNeed = new Handler() {
        public void handleMessage(android.os.Message msg) {
            
        };
    };

    void animateMotionYIfNeed2() {
        hCheckAnimateMotionYIfNeed.sendEmptyMessageDelayed(0, 500);
    }

    void animateMotionYIfNeed() {
        int maxValueOrig = getMaxValueByVisibleFunctionsByPart();
        int minValueOrig = getMinValueByVisibleFunctionsByPart();

        if (data.is2YAxis) {
            maxValueOrig = getMaxValueByVisibleFunctionsByPart(0, 1);
            minValueOrig = getMinValueByVisibleFunctionsByPart(0, 1);
        }

        int maxValue = getMaxValueForBuildChart(maxValueOrig, minValueOrig);
        int minValue = getMinValueForBuildChart(minValueOrig, maxValue);

        boolean needAnimate = lineValues.get(lineValues.size() - 1) != maxValue || lineValues.get(0) != minValue;

        if (data.is2YAxis) {
            if (!data.functions.get(0).isVisible) needAnimate = false;
        }

        Log.d("needAnimate1 = ", ""+needAnimate);

        if (!needAnimate && data.is2YAxis) {
            if (data.functions.get(1).isVisible) {
                maxValueOrig = getMaxValueByVisibleFunctionsByPart(1, 0);
                minValueOrig = getMinValueByVisibleFunctionsByPart(1, 0);

                maxValue = getMaxValueForBuildChart(maxValueOrig, minValueOrig);
                minValue = getMinValueForBuildChart(minValueOrig, maxValue);

                needAnimate = lineValuesRight.get(lineValuesRight.size() - 1) != maxValue || lineValuesRight.get(0) != minValue;

                Log.d("needAnimate2 = ", "" + needAnimate);
            }
        }

        boolean restartAnimation = false;
        
        if (changeMotionYAnimate || changeVisibilityAnimate) {
            if (!needAnimate) {
                return;
            }

            restartAnimation = true;
        }

        valueWasChosenOnChart = false;

        if (needAnimate) {
            recountLineValues(restartAnimation);
            changeChartAnimationIsGrow = lineValuesLast.get(lineValuesLast.size() - 1) < lineValues.get(lineValues.size() - 1);
            changeMotionYAnimationDuration = calcAnimationDuration();

            if (!restartAnimation) {
                startAnimationMotionY();
            } else {
                hAnimVisibility.removeMessages(0);
                hAnimMotionY.removeMessages(0);
                

                startAnimationMotionY();
            }
        }
    }

    long startTimeAnimationLog;

    void startAnimationMotionY() {
        startTimeAnimationLog = System.currentTimeMillis();
        Log.d("animationStart", "motionY");

        changeVisibilityAnimate = false;
        changeMotionYAnimate = true;
        changeMotionYAnimationTime = 0;
        animateMotionY();
    }

    void animateMotionY() {
        invalidate();

        long diff = System.currentTimeMillis() - lastTime;
        lastTime = System.currentTimeMillis();
        if (diff > 60) diff = 60;

        changeMotionYAnimationTime += diff;
        scrollPanelAnimationTime += diff;

        if (changeMotionYAnimationTime < changeMotionYAnimationDuration) {
            hAnimMotionY.sendEmptyMessageDelayed(0, 50);
        } else {
            endAnimationMotionY();
        }

        if (scrollPanelAnimationTime > changeVisibilityAnimationDuration) {
            scrollPanelAnimationTime = changeVisibilityAnimationDuration;
        }
    }

    void endAnimationMotionY() {
        long time = System.currentTimeMillis() - startTimeAnimationLog;
        Log.d("animationEnd", "motionY = "+time);
        changeMotionYAnimate = false;
        changeMotionYAnimationTime = 0;
        scrollPanelAnimationTime = 0;

        changeVisibilityStates();

        animateMotionYIfNeed();
    }

    void generateLineValues() {
        recountLineValues();
    }

    public void turnTheme(boolean isDarkTheme) {
        this.isDarkTheme = isDarkTheme;
        setColors();
        setPaints();
        invalidate();
    }

    void setColors() {
        backgroundColor = "#ffffff";
        chartTitleTextColor = "#000000";
        chartYAxisValueColor = "#252529";
        chartXAxisValueColor = "#96a2aa";
        chartLineColor = "#182D3B";
        rectChosenValueBackgroundColor = "#ffffff";
        rectChosenValueLineColor = "#e5ebef";
        rectChosenValueDateColor = "#222222";
        rectChosenValueBorder1Color = "#dbdbdb";
        rectChosenValueBorder2Color = "#e8e8e8";
        scrollPanelColor = "#f5f8f9";
        scrollPanelBordersColor = "#dbe7f0";

        if (data.type == CHART_TYPE_STACK || data.type == CHART_TYPE_PERCENTAGE) {
            
            scrollPanelBordersColor = "#222222";
        }

        if (isDarkTheme) {
            backgroundColor = "#1d2733";
            chartTitleTextColor = "#ffffff";
            chartYAxisValueColor = "#ECF2F8";
            chartXAxisValueColor = "#506372";
            chartLineColor = "#ffffff";
            rectChosenValueBackgroundColor = "#202b38";
            rectChosenValueLineColor = "#131c26";
            rectChosenValueDateColor = "#e5eff5";
            rectChosenValueBorder1Color = "#18222c";
            rectChosenValueBorder2Color = "#1a232e";
            scrollPanelColor = "#19232e";
            scrollPanelBordersColor = "#2b4256";
        }

        if (data.isDesignV1) {
            setColorsV1();
        }
    }

    void setColorsV1() {
        backgroundColor = "#ffffff";
        chartTitleTextColor = "#3896d4";
        chartYAxisValueColor = "#96a2aa";
        chartXAxisValueColor = "#96a2aa";
        chartLineColor = "#f1f1f2";
        rectChosenValueBackgroundColor = "#ffffff";
        rectChosenValueLineColor = "#e5ebef";
        rectChosenValueDateColor = "#222222";
        rectChosenValueBorder1Color = "#dbdbdb";
        rectChosenValueBorder2Color = "#e8e8e8";
        scrollPanelColor = "#f5f8f9";
        scrollPanelBordersColor = "#dbe7f0";

        if (isDarkTheme) {
            backgroundColor = "#1d2733";
            chartTitleTextColor = "#7bc4fb";
            chartYAxisValueColor = "#506372";
            chartXAxisValueColor = "#506372";
            chartLineColor = "#161f2b";
            rectChosenValueBackgroundColor = "#202b38";
            rectChosenValueLineColor = "#131c26";
            rectChosenValueDateColor = "#e5eff5";
            rectChosenValueBorder1Color = "#18222c";
            rectChosenValueBorder2Color = "#1a232e";
            scrollPanelColor = "#19232e";
            scrollPanelBordersColor = "#2b4256";
        }
    }

    void setPaints() {
        p1 = new Paint();
        p1.setAntiAlias(true);
        p1.setStrokeWidth(dp(2));
        p1.setColor(Color.parseColor("#3cc23f"));

        p2 = new Paint();
        
        p2.setAntiAlias(true);
        p2.setTextSize(dp(14));
        p2.setColor(Color.parseColor(chartYAxisValueColor));
        p2.setAlpha(data.isDesignV1 ? 255 : (int)(0.5 * 255));

        p3 = new Paint();
        p3.setColor(Color.parseColor(chartLineColor));
        
        if (!data.isDesignV1) {
            p3.setAlpha((int) (0.1 * 255));
        }
        p3.setStrokeWidth(dp(1));

        p4 = new Paint();
        p4.setColor(Color.parseColor(backgroundColor));

        p5 = new Paint();
        p5.setAntiAlias(true);
        p5.setStrokeWidth(dp(2));
        p5.setColor(Color.parseColor("#ff0000"));

        p6 = new Paint();
        
        p6.setAntiAlias(true);
        p6.setTextSize(dp(14));
        p6.setTextAlign(Paint.Align.CENTER);
        p6.setColor(Color.parseColor(chartXAxisValueColor));

        p18 = new Paint();
        
        p18.setAntiAlias(true);
        p18.setTextSize(dp(15));
        
        if (!data.isDesignV1) {
            p18.setTypeface(Typeface.DEFAULT_BOLD);
        }
        
        p18.setColor(Color.parseColor(chartTitleTextColor));

        for (int i = 0; i < data.functions.size(); i++) {
            Paint p = new Paint();
            p.setAntiAlias(true);
            p.setStrokeWidth(0);
            Log.d(LOG_TAG, "color = "+data.functions.get(i).color);
            p.setColor(Color.parseColor(data.functions.get(i).color));

            if (data.type == CHART_TYPE_LINE) {
                p.setStyle(Paint.Style.STROKE);
                p.setStrokeWidth(dp(2));
            } else {
                
                p.setStyle(Paint.Style.FILL);
                
                p.setStrokeWidth(0);
            }

            functionPaints.add(p);

            p = new Paint(p);
            p.setStrokeWidth(dp(1));
            
            functionPaintsForScrollPanel.add(p);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        fillBackground(canvas);
        drawScrollPanel(canvas);
        drawChartTitle(canvas);

        if (data.type == CHART_TYPE_LINE && !data.is2YAxis) {
            drawChartLines(canvas);
        }

        drawChart(canvas);

        if (data.type == CHART_TYPE_STACK || data.type == CHART_TYPE_PERCENTAGE || (data.type == CHART_TYPE_LINE && data.is2YAxis)) {
            drawChartLines(canvas);
        }

        fillBackgroundBottomPart(canvas);
        drawDates(canvas);
        drawChosenValueDialogOnChart(canvas);
        drawCustomCheckboxesIfNeed(canvas);
    }

    Handler hCheckLongClick = new Handler() {
        public void handleMessage(android.os.Message msg) {
            checkLongClick();
        };
    };

    void checkLongClick() {
        longClickWasStarted = false;
        if (data.isDesignV1) return;
        if (!touchIsActive) return;

        for (int i = 0; i < customCheckboxesRect.size(); i++) {
            if (i != longClickPos) continue;
            if (customCheckboxesRect.get(i).contains((int)lastMotionX, (int)lastMotionY)) {
                int countUnchecked = 0;
                for (int j = 0; j < customCheckboxesRect.size(); j++) {
                    if (i == j) continue;
                    if (!data.functions.get(j).isVisible) continue;
                    countUnchecked++;
                    customCheckboxesStates.set(j, false);
                    data.functions.get(j).isVisible = false;
                }

                if (customCheckboxesStates.get(i) && countUnchecked == 0) {
                    return;
                }

                Log.d("here111", ""+longClickWasUsed);

                longClickWasUsed = true;
                customCheckboxesStates.set(i, true);
                data.functions.get(i).isVisible = true;

                changeChartState(i, customCheckboxesStates.get(i));

                invalidate();
                return;
            }
        }
    }

    long downTime;
    float downX, downY;
    float lastMotionX, lastMotionY;
    
    boolean longClickWasStarted;
    boolean longClickWasUsed;
    int longClickPos;

    boolean touchIsActive;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.d(LOG_TAG, "onTouchEvent "+event.getAction()+" "+event.getX()+" "+event.getY());

        scaleGestureDetector.onTouchEvent(event);

        lastMotionX = event.getX();
        lastMotionY = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touchIsActive = true;

                downTime = System.currentTimeMillis();
                downX = event.getX();
                downY = event.getY();

                if (event.getY() > scrollAreaY && event.getY() < scrollAreaY + scrollAreaHeight) {
                    float touchArea = dp(10);

                    if (event.getX() - touchArea < scrollAreaX && scrollAreaX < event.getX() + touchArea) {
                        scrollAreaChangeModeLeft = true;
                    } else if (event.getX() - touchArea < scrollAreaX + scrollAreaWidth && scrollAreaX + scrollAreaWidth < event.getX() + touchArea) {
                        scrollAreaChangeModeRight = true;
                    } else {
                        scrollAreaIsCaptured = true;
                    }
                    
                    Log.d(LOG_TAG, "Down chModeL = "+scrollAreaChangeModeLeft+"   chModeR = "+scrollAreaChangeModeRight+"   capt = "+scrollAreaIsCaptured);

                    lastTouchX = event.getX();
                }  

                if (!data.isDesignV1) {
                    for (int i = 0; i < customCheckboxesRect.size(); i++) {
                        if (customCheckboxesRect.get(i).contains((int)event.getX(), (int)event.getY())) {
                            if (!longClickWasStarted) {
                                longClickWasStarted = true;
                                longClickPos = i;
                                hCheckLongClick.sendEmptyMessageDelayed(0, 1000);
                                break;
                            }
                        }
                    }
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (scrollAreaIsCaptured) {
                    scrollAreaX += (event.getX() - lastTouchX);
                    if (scrollAreaX < marginsX) scrollAreaX = marginsX;
                    if (scrollAreaX + scrollAreaWidth > width - marginsX) scrollAreaX = width - marginsX - scrollAreaWidth;
                    updateBeginAndEndPos();
                    animateMotionYIfNeed();
                    lastTouchX = event.getX();
                    invalidate();
                }

                if (scrollAreaChangeModeLeft) {
                    float lastScrollAreaX = scrollAreaX;
                    scrollAreaX = (int)event.getX();
                    if (scrollAreaX < marginsX) scrollAreaX = marginsX;
                    scrollAreaWidth += (lastScrollAreaX - scrollAreaX);
                    if (scrollAreaWidth <= 0) {
                        scrollAreaWidth = 0;
                    }
                    if (scrollAreaWidth < minScrollAreaWidth) {
                        scrollAreaWidth = minScrollAreaWidth;
                    }
                    if (scrollAreaX + scrollAreaWidth > width - marginsX) scrollAreaX = width - marginsX - scrollAreaWidth;
                    updateBeginAndEndPos();
                    animateMotionYIfNeed();
                    invalidate();
                }

                if (scrollAreaChangeModeRight) {
                    Log.d(LOG_TAG, "scrollAreaWidth = "+scrollAreaWidth);
                    scrollAreaWidth = ((int)event.getX() - scrollAreaX);
                    if (scrollAreaWidth <= 0) scrollAreaWidth = 0;
                    if (scrollAreaWidth < minScrollAreaWidth) scrollAreaWidth = minScrollAreaWidth;
                    if (scrollAreaX + scrollAreaWidth > width - marginsX) scrollAreaWidth = width - marginsX - scrollAreaX;
                    updateBeginAndEndPos();
                    animateMotionYIfNeed();
                    invalidate();
                }

                if (!scrollAreaChangeModeLeft && !scrollAreaChangeModeRight && !scrollAreaIsCaptured) {
                    long moveTime = System.currentTimeMillis();
                    if (moveTime - downTime < 500) {
                        
                        if (event.getY() < beginPos && Math.abs(event.getY() - downY) < dp(2) && Math.abs(event.getX() - downX) > dp(5)) {
                            isChoseValueOnChartMode = true;
                            valueWasChosenOnChart = true;
                        }
                    }
                }

                if (isChoseValueOnChartMode) {
                    chosenValueOnChart = (int) (bPos + (event.getX() / (width - (marginsX * 2)) * (ePos - bPos - 1)));

                    if (chosenValueOnChart < ((int)bPos)) chosenValueOnChart = (int)bPos;
                    if (chosenValueOnChart > ((int)ePos)) chosenValueOnChart = (int)ePos;

                    if (chosenValueOnChart < 0) chosenValueOnChart = 0;
                    if (chosenValueOnChart >= data.countElements)
                        chosenValueOnChart = data.countElements - 1;

                    Log.d(LOG_TAG, "chosenValueOnChart = " + chosenValueOnChart);
                    invalidate();
                }

                break;
            case MotionEvent.ACTION_UP:
                if (isZoomEnabled) {
                    if (width - dp(150) < event.getX() && event.getX() < width) {
                        if (event.getY() < height) {
                            valueWasChosenOnChart = false;
                            isZoomed = !isZoomed;
                            if (isZoomed) {
                                formZoomData();
                                recountLineValuesForZoom();
                            } else {
                                recountLineValues();
                            }
                            
                            return true;
                        }
                    }
                }

                boolean modeWasNotChosen = !scrollAreaChangeModeLeft && !scrollAreaChangeModeRight && !scrollAreaIsCaptured && !isChoseValueOnChartMode;

                scrollAreaChangeModeLeft = false;
                scrollAreaChangeModeRight = false;
                scrollAreaIsCaptured = false;
                isChoseValueOnChartMode = false;
                touchIsActive = false;

                Log.d("pointerCount", ""+event.getPointerCount());

                long upTime = System.currentTimeMillis();
                
                if (modeWasNotChosen && valueWasChosenOnChart) {
                    if (upTime - downTime < 500) {
                        if (rectChosenValueDialog != null) {
                            if (rectChosenValueDialog.left < event.getX() && event.getX() < rectChosenValueDialog.right &&
                                rectChosenValueDialog.top < event.getY() && event.getY() < rectChosenValueDialog.bottom) {
                                valueWasChosenOnChart = false;
                                invalidate();
                                return true;
                            }
                        }
                    }
                }
                
                if (event.getY() < beginPos && event.getX() > marginsX && event.getX() < (width - marginsX)) {
                    if (upTime - downTime < 500) {
                        valueWasChosenOnChart = true;
                        chosenValueOnChart = (int) bPos + (int) (event.getX() / (width - (marginsX * 2)) * (ePos - bPos));

                        if (chosenValueOnChart < 0) chosenValueOnChart = 0;
                        if (chosenValueOnChart >= data.countElements)
                            chosenValueOnChart = data.countElements - 1;

                        Log.d(LOG_TAG, "valueWasChosenOnChart true");
                        Log.d(LOG_TAG, "chosenValueOnChart = " + chosenValueOnChart);
                        invalidate();

                        return true;
                    }
                }

                if (!data.isDesignV1) {
                    
                    if (!longClickWasUsed) {
                        for (int i = 0; i < customCheckboxesRect.size(); i++) {
                            if (customCheckboxesRect.get(i).contains((int) event.getX(), (int) event.getY())) {
                                customCheckboxesStates.set(i, !customCheckboxesStates.get(i));
                                changeChartState(i, customCheckboxesStates.get(i));
                                
                                break;
                            }
                        }
                    }
                }

                longClickWasStarted = false;
                longClickWasUsed = false;
                hCheckLongClick.removeMessages(0);

                break;
        }
        return true;
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        float mScaleFactor;

        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector){
            Log.d("onScale", "sF = "+scaleGestureDetector.getScaleFactor());
            mScaleFactor *= scaleGestureDetector.getScaleFactor();
            Log.d("mScaleFactor", "sF = "+mScaleFactor);
            if (mScaleFactor > 2) {
                isZoomed = true;
            }
            return true;
        }
    }

    boolean isScrollEnabled() {
        return !scrollAreaChangeModeLeft && !scrollAreaChangeModeRight && !scrollAreaIsCaptured && !isChoseValueOnChartMode;
    }

    void formZoomData() {
        if (!data.isSupportZoom) return;

        bPosZoom = 0;
        ePosZoom = 20;
        data.dataZoomed = data.allDataZoomed.get(2);
    }

    void updateBeginAndEndPos() {        
        if (data.functions.size() == 0) return;
        
        int countElements = data.countElements;
        float currentCountElements = (float)scrollAreaWidth * (data.countElements - 1) / (width - (marginsX * 2));

        bPos = (scrollAreaX - marginsX) * (countElements - 1) / (float) (width - (marginsX * 2));
        ePos = bPos + currentCountElements + 1;
        if (ePos > countElements) {
            ePos = countElements;
            bPos = ePos - currentCountElements - 1;
        }
        if (bPos < 0) bPos = 0;
        int ePos = (scrollAreaX - marginsX + scrollAreaWidth) * countElements / (width - (marginsX * 2));
    }

    void fillBackground(Canvas canvas) {
        canvas.drawColor(Color.parseColor(backgroundColor));
    }

    Paint pBg;

    void fillBackgroundBottomPart(Canvas canvas) {
        if (pBg == null) {
            pBg = new Paint();
            pBg.setColor(Color.parseColor(backgroundColor));
        }
    }

    void drawCustomCheckboxesIfNeed(Canvas canvas) {
        if (data.isDesignV1) return;
        if (data.functions.size() == 1) return;

        boolean isNeedSaveValues = customCheckboxesRect.size() == 0;

        int arcWidth = (int)dp(50);
        int height = (int)dp(45);

        int width = (int)dp(100);
        float x = dp(15);
        float y = beginPos + dp(120);

        Paint p2 = new Paint();
        p2.setStyle(Paint.Style.STROKE);
        p2.setTextSize(dp(17));
        p2.setTextAlign(Paint.Align.LEFT);
        p2.setTypeface(Typeface.DEFAULT_BOLD);
        p2.setAntiAlias(true);
        p2.setColor(Color.parseColor("#ffffff"));

        Paint.FontMetrics fm = p2.getFontMetrics();
        float heightText = (fm.descent - fm.ascent) / 2;

        for (int i = 0; i < data.functions.size(); i++) {
            if (isNeedSaveValues) customCheckboxesStates.add(true);
            boolean isActive = customCheckboxesStates.get(i);

            Rect bounds = new Rect();
            p2.getTextBounds(data.functions.get(i).name, 0, data.functions.get(i).name.length(), bounds);
            float widthText = bounds.width();
            width = (int)(widthText * 1.2);
            if (this.width - x < width + arcWidth) {
                x = dp(15);
                y += height + dp(15);
            }

            Paint p = new Paint();
            p.setAntiAlias(true);
            p.setStyle(Paint.Style.FILL);
            p.setColor(Color.parseColor(data.functions.get(i).color));

            float border = dp(1);
            canvas.drawRect(x + (arcWidth / 2), y, x + (arcWidth / 2) + width , y + height, p);

            float radius = height;

            canvas.drawRoundRect(new RectF(x, y, x + arcWidth, y + height), radius, radius, p);
            canvas.drawRoundRect(new RectF(x + width, y, x + width + arcWidth, y + height), radius, radius, p);

            if (!isActive) {
                p.setColor(Color.parseColor(backgroundColor));
                canvas.drawRoundRect(new RectF(x + border, y + border, x + arcWidth - border, y + height - border), radius, radius, p);
                canvas.drawRoundRect(new RectF(x + width + border, y + border, x + width + arcWidth - border, y + height - border), radius, radius, p);
            }

            if (!isActive) {
                p.setColor(Color.parseColor(backgroundColor));
                canvas.drawRect(x + (arcWidth / 2) + border, y + border, x + (arcWidth / 2) + width - border, y + height - border, p);
            }

            if (isNeedSaveValues) {
                
                customCheckboxesRect.add(new Rect((int)(x), (int)(y), (int)(x + width + arcWidth), (int)(y + height)));
            }

            float yCheckShift = height * 0.03f;

            if (isActive) {
                float checkMargin = dp(15);

                p2.setTextAlign(Paint.Align.LEFT);
                p2.setColor(Color.WHITE);
                canvas.drawText(data.functions.get(i).name, x + dp(37), y + (height / 2) + (heightText / 2) + yCheckShift, p2);

                yCheckShift = 0;

                Paint p3 = new Paint();
                p3.setStyle(Paint.Style.STROKE);
                p3.setStrokeWidth(dp(3));
                p3.setAntiAlias(true);
                p3.setColor(Color.WHITE);

                Path check = new Path();
                check.moveTo(x + checkMargin, y + (height / 2) + (height * 0.03f) - yCheckShift);
                check.lineTo(x + checkMargin + dp(5), y + (height / 2) + ((heightText) / 2) - yCheckShift);
                check.lineTo(x + checkMargin + dp(15), y + (height / 2) - (heightText / 2) - yCheckShift);
                canvas.drawPath(check, p3);
            } else {
                p2.setTextAlign(Paint.Align.CENTER);
                p2.setColor(Color.parseColor(data.functions.get(i).color));
                canvas.drawText(data.functions.get(i).name, x + (arcWidth / 2) + (width / 2), y + (height / 2) + (heightText / 2) + yCheckShift, p2);
            }
            x += width + (arcWidth / 2) + dp(35);
        }
    }

    int getCustomCheckboxPanelHeight() {
        if (data.isDesignV1) return 0;
        if (data.functions.size() == 1) return 0;

        int radius = (int)dp(10);
        int arcWidth = (int)dp(50);
        int height = (int)dp(45);

        int width = (int)dp(100);
        float x = dp(15);
        float y = beginPos + dp(120);

        float heightPanel = height + dp(15);

        Paint p2 = new Paint();
        p2.setStyle(Paint.Style.STROKE);
        p2.setTextSize(dp(17));
        p2.setTextAlign(Paint.Align.LEFT);
        p2.setTypeface(Typeface.DEFAULT_BOLD);
        p2.setColor(Color.parseColor("#ffffff"));

        Paint.FontMetrics fm = p2.getFontMetrics();
        float heightText = (fm.descent - fm.ascent) / 2;

        for (int i = 0; i < data.functions.size(); i++) {
            Rect bounds = new Rect();
            p2.getTextBounds(data.functions.get(i).name, 0, data.functions.get(i).name.length(), bounds);
            float widthText = bounds.width();
            width = (int) (widthText * 1.2);

            if (this.width - x < width + arcWidth) {
                x = dp(15);
                y += height + dp(15);
                heightPanel += (height + dp(15));
                
            }
            x += width + (arcWidth / 2) + dp(35);
        }
        return (int)heightPanel;
    }

    void drawChartTitle(Canvas canvas) {
        if (data.isDesignV1) {
            drawChartTitleV1(canvas);
            return;
        }
        p18.setTextSize(dp(16));
        canvas.drawText(chartTitle, marginsX, beginPos - dp(288), p18);

        int datePos = (int)(bPos);
        if (datePos < 0) datePos = 0;
        String dateTitle = String.valueOf(data.datesWithYear.get(datePos))+" - ";
        datePos = (int)(ePos);
        if (datePos >= data.datesWithYear.size()) datePos = data.datesWithYear.size() - 1;
        dateTitle += String.valueOf(data.datesWithYear.get(datePos));

        p18.setTextSize(dp(14));
        p18.setTextAlign(Paint.Align.RIGHT);
        canvas.drawText(dateTitle, width - marginsX, beginPos - dp(288), p18);
        p18.setTextAlign(Paint.Align.LEFT);
    }

    void drawChartTitleV1(Canvas canvas) {
        canvas.drawText(chartTitle, marginsX, beginPos - dp(287), p18);
    }

    void drawChartLines(Canvas canvas) {
        if (false) {
            drawChartLinesOnAnimations(canvas);
        } else {
            boolean needDrawText = false;

            for (int i = 0; i < data.functions.size(); i++) {
                if (data.functions.get(i).isVisible) {
                    needDrawText = true;
                }
            }

            if (data.type == CHART_TYPE_PERCENTAGE) {
                int yShift = this.chartMaxHeight / 5;
                int beginPos = this.beginPos;
                int textMarginY = (int) dp(5);
                for (int i = 0; i < 5; i++) {
                    if (needDrawText) {
                        canvas.drawText(String.valueOf(25 * i), marginsX, beginPos - (yShift * i) - textMarginY, p2);
                    }
                    canvas.drawLine(marginsX, beginPos - (yShift * i), width - marginsX, beginPos - (yShift * i), p3);
                }
            } else {
                int yShift = this.chartMaxHeight / 6;
                int beginPos = this.beginPos;
                int textMarginY = (int) dp(5);

                if (!data.is2YAxis) {
                    for (int i = 0; i < 6; i++) {
                        if (needDrawText) {
                            canvas.drawText(formatNumberIfNeed(lineValues.get(i)), marginsX, beginPos - (yShift * i) - textMarginY, p2);
                        }
                        canvas.drawLine(marginsX, beginPos - (yShift * i), width - marginsX, beginPos - (yShift * i), p3);
                    }
                } else {
                    Paint p2l = new Paint(this.p2);
                    p2l.setTextAlign(Paint.Align.LEFT);
                    p2l.setColor(Color.parseColor(data.functions.get(0).color));

                    Paint p2r = new Paint(this.p2);
                    p2r.setTextAlign(Paint.Align.RIGHT);
                    p2r.setColor(Color.parseColor(data.functions.get(1).color));

                    for (int i = 0; i < 6; i++) {

                        if (data.functions.get(0).isVisible) {
                            canvas.drawText(formatNumberIfNeed(lineValues.get(i)), marginsX, beginPos - (yShift * i) - textMarginY, p2l);
                        }

                        if (data.functions.get(1).isVisible) {
                            canvas.drawText(formatNumberIfNeed(lineValuesRight.get(i)), width - marginsX, beginPos - (yShift * i) - textMarginY, p2r);
                        }

                        canvas.drawLine(marginsX, beginPos - (yShift * i), width - marginsX, beginPos - (yShift * i), p3);
                    }
                }

            }
        }
    }

    void drawChartLinesOnAnimations(Canvas canvas) {
        float percentOfAnimation = 0;

        if (changeVisibilityAnimate) {
            percentOfAnimation = changeVisibilityAnimationTime / (float)changeVisibilityAnimationDuration;
        }

        if (changeMotionYAnimate) {
            percentOfAnimation = changeMotionYAnimationTime / (float)changeMotionYAnimationDuration;
        }


        int sizeBetweenLines = lineValues.get(lineValues.size() - 1) - lineValues.get(lineValues.size() - 2);
        int maxValue = lineValues.get(lineValues.size() - 1) + sizeBetweenLines;

        int sizeBetweenLines1 = lineValuesLast.get(lineValuesLast.size() - 1) - lineValuesLast.get(lineValuesLast.size() - 2);
        int maxValue1 = lineValuesLast.get(lineValuesLast.size() - 1) + sizeBetweenLines1;

        float heightLast = (((float)maxValue1 / maxValue * dp(50)) - dp(50));
        
        float heightNew = ((float)maxValue / maxValue1 * dp(50));

        float yShiftLast, yShift;

        if (!changeChartAnimationIsGrow) {
            yShiftLast = ((int) dp(100) * percentOfAnimation) + dp(50);
            yShift = ((int) dp(30) * percentOfAnimation) + dp(20);

            yShiftLast = (heightLast * percentOfAnimation) + dp(50);
            yShift = ((dp(50) - heightNew) * percentOfAnimation) + heightNew;
        } else {
            yShift = ((int) dp(100) * (1 - percentOfAnimation)) + dp(50);
            yShiftLast = ((int) dp(30) * (1 - percentOfAnimation)) + dp(20);

            heightLast = (((float)maxValue / maxValue1 * dp(50)) - dp(50));
            heightNew = ((float)maxValue1 / maxValue * dp(50));

            yShift = (heightLast * (1 - percentOfAnimation)) + dp(50);
            yShiftLast = ((dp(50) - heightNew) * (1 - percentOfAnimation)) + heightNew;
        }

        float yShiftL = chartMaxHeight / 6;
        int beginPos = this.beginPos;
        int textMarginY = (int) dp(5);
        int fullPercent = data.isDesignV1 ? 255 : ((int)(0.5 * 255));

        for (int i = 0; i < 6; i++) {
            canvas.drawLine(marginsX, beginPos - (yShiftL * i), width - marginsX, beginPos - (yShiftL * i), p3);
            p2.setAlpha((int)(percentOfAnimation * fullPercent));
            if (lineValues.get(5) != 5) {
                canvas.drawText(formatNumberIfNeed(lineValues.get(i)), marginsX, beginPos - (yShift * i) - textMarginY, p2);
            }
            p2.setAlpha(fullPercent - (int) (percentOfAnimation * fullPercent));
            if (lineValuesLast.get(5) != 5) {
                canvas.drawText(formatNumberIfNeed(lineValuesLast.get(i)), marginsX, beginPos - (yShiftLast * i) - textMarginY, p2);
            }
        }

        p2.setAlpha(fullPercent);
    }

    boolean pathWasForm;
    boolean needUpdatePathSP;
    int needUpdatePathSPCount;
    
    ArrayList<Path> paths = new ArrayList<>();
    ArrayList<Path> pathsOriginal = new ArrayList<>();
    ArrayList<Matrix> matrices = new ArrayList<>();

    int maxValueLeftSPLast, minValueLeftSPLast, maxValueRightSPLast, minValueRightSPLast;

    void formPath(ArrayList<Path> paths, boolean isScrollPanelMode) {
        float bPos = this.bPos;
        float xShift = (width - (marginsX * 2)) / (ePos - bPos - 1);

        if (data.type == CHART_TYPE_STACK) {
            xShift = (width - (marginsX * 2)) / (ePos - bPos);
        }

        float countElementsForBorders = (marginsX) / xShift;
        if (countElementsForBorders < 3) countElementsForBorders = 3;
        countElementsForBorders++;

        int beginPos = this.beginPos;

        int sizeBetweenLines = lineValues.get(lineValues.size() - 1) - lineValues.get(lineValues.size() - 2);
        int maxValue = lineValues.get(lineValues.size() - 1) + sizeBetweenLines;
        int minValue = lineValues.get(0);

        int maxValueLeft = 0, minValueLeft = 0, maxValueRight = 0, minValueRight = 0;

        if (data.is2YAxis) {
            maxValueLeft = maxValue;
            minValueLeft = minValue;

            int sizeBetweenLinesRight = lineValuesRight.get(lineValuesRight.size() - 1) - lineValuesRight.get(lineValuesRight.size() - 2);
            maxValueRight = lineValuesRight.get(lineValuesRight.size() - 1) + sizeBetweenLinesRight;
            minValueRight = lineValuesRight.get(0);
        }

        float chartMaxHeight = this.chartMaxHeight;

        int maxValue1 = maxValue;
        int minValue1 = minValue;

        int maxValue1Left = 0, minValue1Left = 0, maxValue1Right = 0, minValue1Right = 0;

        if (data.is2YAxis) {
            maxValue1Left = maxValue;
            minValue1Left = minValue;

            maxValue1Right = maxValueRight;
            minValue1Right = minValueRight;
        }

        if (changeVisibilityAnimate || changeMotionYAnimate) {
            int sizeBetweenLines1 = lineValuesLast.get(lineValuesLast.size() - 1) - lineValuesLast.get(lineValuesLast.size() - 2);
            maxValue1 = lineValuesLast.get(lineValuesLast.size() - 1) + sizeBetweenLines1;
            minValue1 = lineValuesLast.get(0);

            if (data.is2YAxis && lineValuesRightLast.size() > 0) {
                sizeBetweenLines1 = lineValuesRightLast.get(lineValuesRightLast.size() - 1) - lineValuesRightLast.get(lineValuesRightLast.size() - 2);
                maxValue1Right = lineValuesRightLast.get(lineValuesRightLast.size() - 1) + sizeBetweenLines1;
                minValue1Right = lineValuesRightLast.get(0);
            }

            if (maxValue == 6) {
                maxValue = maxValue1;
                minValue = minValue1;
            }

            if (maxValue1 == 6) {
                maxValue1 = maxValue;
                minValue1 = minValue;
            }

            if (maxValueRight == 6) {
                
                maxValueRight = maxValue1Right;
                minValueRight = minValue1Right;
            }

            maxValueLeft = maxValue;
            minValueLeft = minValue;

            maxValue1Left = maxValue1;
            minValue1Left = minValue1;
        }

        if (!isScrollPanelMode) {
            Log.d("formPath", "bP = "+bPos+"   eP = "+ePos+"   xSh = "+xShift);
        }

        if (isScrollPanelMode) {
            xShift = (width - (marginsX * 2)) / ((float)data.countElements - 1);

            if (data.type == CHART_TYPE_STACK) {
                xShift = (width - (marginsX * 2)) / ((float)data.countElements);
            }

            beginPos = this.beginPos + (int)dp(100);

            int maxValueOrig = getMaxValueByVisibleFunctions();
            int minValueOrig = getMinValueByVisibleFunctions();

            if (data.is2YAxis) {
                maxValueOrig = getMaxValueByVisibleFunctions(0, 1);
                minValueOrig = getMinValueByVisibleFunctions(0, 1);
            }

            maxValue = getMaxValueForBuildChart(maxValueOrig, minValueOrig);
            minValue = getMinValueForBuildChart(minValueOrig, maxValue);

            maxValue = minValue + ((maxValue - minValue) * 6 / 5);

            Log.d("FPmaxValue", ""+maxValue);

            if (data.is2YAxis) {
                maxValueOrig = getMaxValueByVisibleFunctions(1, 0);
                minValueOrig = getMinValueByVisibleFunctions(1, 0);

                maxValueRight = getMaxValueForBuildChart(maxValueOrig, minValueOrig);
                minValueRight = getMinValueForBuildChart(minValueOrig, maxValueRight);

                maxValueRight = minValueRight + ((maxValueRight - minValueRight) * 6 / 5);
            }

            if (maxValue == 6 || maxValue == 5) {
                
                maxValue = maxValue1;
                minValue = minValue1;
            }

            if (maxValue1 == 6 || maxValue1 == 5) {
                maxValue1 = maxValue;
                minValue1 = minValue;
            }

            if (maxValueRight == 6 || maxValueRight == 5) {
                
                maxValueRight = maxValue1Right;
                minValueRight = minValue1Right;
            }

            maxValueLeft = maxValue;
            minValueLeft = minValue;

            chartMaxHeight = (int)dp(50);

            bPos = 0;

            maxValue1Left = maxValueLeftSPLast;
            minValue1Left = minValueLeftSPLast;

            maxValue1Right = maxValueRightSPLast;
            minValue1Right = minValueRightSPLast;

            maxValue1 = maxValue1Left;
            minValue1 = minValue1Left;


            maxValueLeftSPLast = maxValueLeft;
            minValueLeftSPLast = minValueLeft;

            maxValueRightSPLast = maxValueRight;
            minValueRightSPLast = minValueRight;
        }

        if (!isScrollPanelMode) {
            formPathWasMaxValue = maxValue;
        }

        float x1, y1, x2, y2, y1SP, y2SP;

        int bPosInFor = (int) (bPos - countElementsForBorders);
        int ePosInFor = (int) (ePos + countElementsForBorders);

        if (bPosInFor < 0) bPosInFor = 0;
        if (ePosInFor > data.countElements) ePosInFor = data.countElements;

        int countElements = data.countElements;
        float bPosF = (scrollAreaX - marginsX) * ((float) countElements - 1) / (width - (marginsX * 2));

        double value = bPosF;
        double fractionalPart = value % 1;

        float a = 1 - (float) fractionalPart;

        float shiftFractal = (float) fractionalPart * xShift;

        shiftFractal = 0;

        if (isScrollPanelMode) {
            bPosInFor = 1;
            ePosInFor = data.countElements;
        }

        paths.clear();
        matrices.clear();

        if (paths.size() == 0) {
            for (int i = 0; i < data.functions.size(); i++) {
                paths.add(new Path());
                
                
                matrices.add(new Matrix());
            }
        } else {
            for (int i = 0; i < paths.size(); i++) {
                paths.get(i).reset();
                matrices.get(i).reset();
            }
        }

        float by = beginPos;
        float by2 = beginPos;

        if (data.type == CHART_TYPE_LINE) {

            if (bPosInFor == 0) bPosInFor = 1;

            for (int i = bPosInFor; i < ePosInFor; i++) {
                x1 = marginsX - shiftFractal + xShift * (i - bPos - 1);
                x2 = marginsX - shiftFractal + xShift * (i - bPos);

                for (int j = 0; j < data.functions.size(); j++) {
                    if ((changeVisibilityAnimate && data.functions.get(j).needChangeVisibility) || data.functions.get(j).isVisible) {

                        if (data.is2YAxis) {
                            maxValue = j == 0 ? maxValueLeft : maxValueRight;
                            minValue = j == 0 ? minValueLeft : minValueRight;

                            maxValue1 = j == 0 ? maxValue1Left : maxValue1Right;
                            minValue1 = j == 0 ? minValue1Left : minValue1Right;
                        }

                        y1 = (int) (beginPos - (((data.functions.get(j).points.get(i - 1) - minValue) / (float) (maxValue - minValue)) * chartMaxHeight));
                        y2 = (int) (beginPos - (((data.functions.get(j).points.get(i) - minValue) / (float) (maxValue - minValue)) * chartMaxHeight));

                        if (changeVisibilityAnimate || changeMotionYAnimate) {
                            
                                PointF pointF = calcAnim(maxValue, minValue, maxValue1, minValue1, j, i, beginPos, chartMaxHeight);
                                y1 = pointF.x;
                                y2 = pointF.y;
                            
                        }

                        if (i == bPosInFor) {
                            paths.get(j).moveTo(x1, y1);
                        }

                        paths.get(j).lineTo(x2, y2);
                    }
                }
            }
        }

        if (data.type == CHART_TYPE_STACK) {
            if (bPosInFor == 0) bPosInFor = 1;
            ePosInFor += 1;

            for (int i = bPosInFor; i < ePosInFor; i++) {

                x1 = marginsX - shiftFractal + xShift * (i - bPos - 1);
                x2 = marginsX - shiftFractal + xShift * (i - bPos);

                by = beginPos;

                for (int j = 0; j < data.functions.size(); j++) {
                    if (data.functions.get(j).isVisible) {
                        y1 = (int) (by - (((data.functions.get(j).points.get(i - 1) - minValue) / (float) (maxValue - minValue)) * chartMaxHeight));

                        if (changeMotionYAnimate) {
                            PointF pointF = calcAnim(maxValue, minValue, maxValue1, minValue1, j, i, by, chartMaxHeight);
                            y1 = pointF.x;
                        }

                        if (i == bPosInFor) {
                            paths.get(j).moveTo(x1, y1);
                        }

                        paths.get(j).lineTo(x1, y1);
                        paths.get(j).lineTo(x2, y1);

                        by = y1;
                    }
                }
            }

            for (int i = ePosInFor - 1; i >= bPosInFor; i--) {

                x1 = marginsX - shiftFractal + xShift * (i - bPos - 1);
                x2 = marginsX - shiftFractal + xShift * (i - bPos);

                by = beginPos;
                

                for (int j = 0; j < data.functions.size(); j++) {
                    if (data.functions.get(j).isVisible) {
                        y1 = (int) (by - (((data.functions.get(j).points.get(i - 1) - minValue) / (float) (maxValue - minValue)) * chartMaxHeight));

                        if (changeMotionYAnimate) {
                            PointF pointF = calcAnim(maxValue, minValue, maxValue1, minValue1, j, i, by, chartMaxHeight);
                            y1 = pointF.x;
                            
                        }

                        paths.get(j).lineTo(x2, by);
                        paths.get(j).lineTo(x1, by);

                        by = y1;
                    }
                }
            }
        }
        
        if (data.type == CHART_TYPE_PERCENTAGE) {

            if (bPosInFor == 0) bPosInFor = 1;

            
            if (!isScrollPanelMode) {
                chartMaxHeight = chartMaxHeight * 4 / 5 + dp(0.5f);
            }

            int time = 0;
            int duration = 1;

            if (changeVisibilityAnimate) {
                time = changeVisibilityAnimationTime;
                duration = changeVisibilityAnimationDuration;
            }

            float percentOfAnimation = (float)time / duration;
            if (percentOfAnimation > 1) percentOfAnimation = 1;
            float percentOfAnimationFinal = 0;
            boolean isGrowAnimation = false;

            for (int j = 0; j < data.functions.size(); j++) {
                if (data.functions.get(j).needChangeVisibility) {
                    percentOfAnimationFinal = !data.functions.get(j).isVisible ? (1 - percentOfAnimation) : percentOfAnimation;
                    isGrowAnimation = !data.functions.get(j).isVisible;
                }
            }

            if (isScrollPanelMode) {
                if (isGrowAnimation) percentOfAnimationFinal = 0;
            }

            for (int i = bPosInFor; i < ePosInFor; i++) {

                x1 = marginsX - shiftFractal + xShift * (i - bPos - 1);
                x2 = marginsX - shiftFractal + xShift * (i - bPos);

                maxValue = 0;
                minValue = 0;

                int maxValue2 = 0;
                int minValue2 = 0;

                for (int j = 0; j < data.functions.size(); j++) {
                    if (changeVisibilityAnimate && data.functions.get(j).needChangeVisibility) {
                        maxValue += (data.functions.get(j).points.get(i - 1) * percentOfAnimationFinal);
                        maxValue2 += (data.functions.get(j).points.get(i) * percentOfAnimationFinal);
                    } else {
                        if (!data.functions.get(j).isVisible) continue;
                        maxValue += data.functions.get(j).points.get(i - 1);
                        maxValue2 += data.functions.get(j).points.get(i);
                    }
                }

                by = beginPos;
                by2 = beginPos;

                float currentValue;
                float currentValue2;

                for (int j = 0; j < data.functions.size(); j++) {
                    
                    if ((changeVisibilityAnimate && data.functions.get(j).needChangeVisibility) || data.functions.get(j).isVisible) {
                        if (changeVisibilityAnimate && data.functions.get(j).needChangeVisibility) {
                            currentValue = data.functions.get(j).points.get(i - 1) * percentOfAnimationFinal;
                            currentValue2 = data.functions.get(j).points.get(i) * percentOfAnimationFinal;
                        } else {
                            currentValue = data.functions.get(j).points.get(i - 1);
                            currentValue2 = data.functions.get(j).points.get(i);
                        }

                        y1 = (int) (by - (((currentValue - minValue) / (float) (maxValue - minValue)) * chartMaxHeight));
                        y2 = (int) (by2 - (((currentValue2 - minValue2) / (float) (maxValue2 - minValue2)) * chartMaxHeight));

                        if (j == data.functions.size() - 1) {
                            y1 = (int) (beginPos - chartMaxHeight);
                            y2 = (int) (beginPos - chartMaxHeight);
                        }

                        if (i == bPosInFor) {
                            paths.get(j).moveTo(x1, y1);
                        }

                        paths.get(j).lineTo(x1, y1);
                        paths.get(j).lineTo(x2, y2);

                        by = y1;
                        by2 = y2;
                    }
                }
            }

            for (int i = ePosInFor - 1; i >= bPosInFor; i--) {
                x1 = marginsX - shiftFractal + xShift * (i - bPos - 1);
                x2 = marginsX - shiftFractal + xShift * (i - bPos);

                maxValue = 0;
                minValue = 0;

                int maxValue2 = 0;
                int minValue2 = 0;

                for (int j = 0; j < data.functions.size(); j++) {
                    if (changeVisibilityAnimate && data.functions.get(j).needChangeVisibility) {
                        maxValue += (data.functions.get(j).points.get(i - 1) * percentOfAnimationFinal);
                        maxValue2 += (data.functions.get(j).points.get(i) * percentOfAnimationFinal);
                    } else {
                        if (!data.functions.get(j).isVisible) continue;
                        maxValue += data.functions.get(j).points.get(i - 1);
                        maxValue2 += data.functions.get(j).points.get(i);
                    }
                }

                by = beginPos;
                by2 = beginPos;

                float currentValue;
                float currentValue2;

                for (int j = 0; j < data.functions.size(); j++) {
                    
                    if ((changeVisibilityAnimate && data.functions.get(j).needChangeVisibility) || data.functions.get(j).isVisible) {
                        if (changeVisibilityAnimate && data.functions.get(j).needChangeVisibility) {
                            currentValue = data.functions.get(j).points.get(i - 1) * percentOfAnimationFinal;
                            currentValue2 = data.functions.get(j).points.get(i) * percentOfAnimationFinal;
                        } else {
                            currentValue = data.functions.get(j).points.get(i - 1);
                            currentValue2 = data.functions.get(j).points.get(i);
                        }

                        y1 = (int) (by - (((currentValue - minValue) / (float) (maxValue - minValue)) * chartMaxHeight));
                        y2 = (int) (by2 - (((currentValue2 - minValue2) / (float) (maxValue2 - minValue2)) * chartMaxHeight));

                        if (j == data.functions.size() - 1) {
                            y1 = (int) (beginPos - chartMaxHeight);
                            y2 = (int) (beginPos - chartMaxHeight);
                        }

                        paths.get(j).lineTo(x2, by2);
                        paths.get(j).lineTo(x1, by);

                        by = y1;
                        by2 = y2;
                    }
                }
            }
        }
    }

    float lastOffset, lastOffsetX, lastOffsetY;
    int pathWasCalcByScrollAreaWidth;
    int pathWasCalcByMaxValue;
    int pathWasCalcByMinValue;
    int pathWasCalcByMinValueFirst;
    float lastDiffY;
    float formPathWasMaxValue;
    boolean isPathWasDrawFirstTime;

    final int CHART_TYPE_LINE = 0;
    final int CHART_TYPE_STACK = 2;
    final int CHART_TYPE_PERCENTAGE = 3;

    void changePathsIfNeed() {
        if (changeVisibilityAnimate) {
            if (data.type == CHART_TYPE_STACK || data.type == CHART_TYPE_PERCENTAGE) {
                formPath(paths, false);
                isPathWasDrawFirstTime = false;
            }
        }

        float xShift = (width - (marginsX * 2)) / (ePos - bPos - 1);
        float offsetX = -(bPos * xShift);

        for (int i = 0; i < paths.size(); i++) {
            paths.get(i).offset(-lastOffsetX, 0);
            paths.get(i).offset(offsetX, 0);
        }

        lastOffsetX = offsetX;

        if (pathWasCalcByScrollAreaWidth == 0) {
            pathWasCalcByScrollAreaWidth = scrollAreaWidth;
        }

        if (scrollAreaWidth != pathWasCalcByScrollAreaWidth) {
            float diffX = pathWasCalcByScrollAreaWidth / (float) scrollAreaWidth;
            Log.d("diffX", "" + diffX + "  marginsX + offsetX = " + (marginsX + offsetX));

            for (int i = 0; i < paths.size(); i++) {
                matrices.get(i).reset();
                matrices.get(i).setScale(diffX, 1, marginsX + offsetX, 0);
                paths.get(i).transform(matrices.get(i));
            }
            pathWasCalcByScrollAreaWidth = scrollAreaWidth;
        }

        int sizeBetweenLines = lineValues.get(lineValues.size() - 1) - lineValues.get(lineValues.size() - 2);
        int maxValue = lineValues.get(lineValues.size() - 1) + sizeBetweenLines;
        int minValue = lineValues.get(0);

        if (data.type == CHART_TYPE_STACK || data.type == CHART_TYPE_PERCENTAGE) {
            minValue = 0;
        }

        if (!isPathWasDrawFirstTime) {
            if (!isPathWasDrawFirstTime) {
                pathWasCalcByMaxValue = maxValue;
                pathWasCalcByMinValue = 0;
                pathWasCalcByMinValueFirst = minValue;
                lastDiffY = 1;
            }

            isPathWasDrawFirstTime = true;

            if (maxValue != pathWasCalcByMaxValue || minValue != pathWasCalcByMinValue) {
                float diffY = (formPathWasMaxValue) / (float) (maxValue - minValue);
                float aaa = (formPathWasMaxValue - maxValue) / (float) (maxValue - minValue) * chartMaxHeight;
                float offsetY = ((diffY * chartMaxHeight) - chartMaxHeight) - aaa;

                for (int i = 0; i < paths.size(); i++) {
                    paths.get(i).offset(0, -lastOffsetY);

                    matrices.get(i).reset();
                    matrices.get(i).setScale(1, 1 / lastDiffY, 0, beginPos);
                    paths.get(i).transform(matrices.get(i));

                    matrices.get(i).reset();
                    matrices.get(i).setScale(1, diffY, 0, beginPos);
                    paths.get(i).transform(matrices.get(i));
                    paths.get(i).offset(0, offsetY);
                }

                pathWasCalcByMaxValue = maxValue;
                pathWasCalcByMinValue = minValue;
                lastOffsetY = offsetY;
                lastDiffY = diffY;
            }
        }

        if (changeVisibilityAnimate || changeMotionYAnimate) {
            if (data.type != CHART_TYPE_PERCENTAGE && data.type != CHART_TYPE_STACK) {
                Point maxMin = getIntermediateMaxMinValues();
                maxValue = maxMin.x;
                minValue = maxMin.y;

                if (data.type == CHART_TYPE_STACK || data.type == CHART_TYPE_PERCENTAGE) {
                    minValue = 0;
                }

                float diffY = (formPathWasMaxValue) / (float) (maxValue - minValue);
                float aaa = (formPathWasMaxValue - maxValue) / (float) (maxValue - minValue) * chartMaxHeight;
                float offsetY = ((diffY * chartMaxHeight) - chartMaxHeight) - aaa;

                for (int i = 0; i < paths.size(); i++) {
                    paths.get(i).offset(0, -lastOffsetY);
                    matrices.get(i).reset();

                    matrices.get(i).postScale(1, 1 / lastDiffY, 0, beginPos);
                    paths.get(i).transform(matrices.get(i));
                    matrices.get(i).reset();

                    matrices.get(i).postScale(1, diffY, 0, beginPos);
                    paths.get(i).transform(matrices.get(i));
                    paths.get(i).offset(0, offsetY);
                }

                pathWasCalcByMaxValue = maxValue;
                pathWasCalcByMinValue = minValue;
                lastOffsetY = offsetY;
                lastDiffY = diffY;
            }
        }
    }

    boolean wasEndDrawChartByPath = true;

    void drawChart(Canvas canvas) {
        if (isZoomEnabled) {
            if (isZoomed) {
                drawChart(canvas, data.dataZoomed, true);
                return;
            }
        }
        drawChart(canvas, data, false);
    }

    void drawChart(Canvas canvas, ChartData data, boolean isZoomed) {
        if (!pathWasForm) {
            pathWasForm = true;

            formPath(pathsForScrollPanel, true);

            invalidate();
            return;
        }
        
        float xShift = (width - (marginsX * 2)) / (ePos - bPos - 1);

        if (data.type == CHART_TYPE_STACK) {
            xShift = (width - (marginsX * 2)) / (ePos - bPos);
        }

        float countElementsForBorders = (marginsX) / xShift;
        if (countElementsForBorders < 3) countElementsForBorders = 3;
        countElementsForBorders++;

        int beginPos = this.beginPos;

        int sizeBetweenLines = lineValues.get(lineValues.size() - 1) - lineValues.get(lineValues.size() - 2);
        int maxValue = lineValues.get(lineValues.size() - 1) + sizeBetweenLines;
        int minValue = lineValues.get(0);

        int maxValueLeft = 0, minValueLeft = 0, maxValueRight = 0, minValueRight = 0;

        if (data.is2YAxis) {
            maxValueLeft = maxValue;
            minValueLeft = minValue;

            int sizeBetweenLinesRight = lineValuesRight.get(lineValuesRight.size() - 1) - lineValuesRight.get(lineValuesRight.size() - 2);
            maxValueRight = lineValuesRight.get(lineValuesRight.size() - 1) + sizeBetweenLinesRight;
            minValueRight = lineValuesRight.get(0);
        }

        float chartMaxHeight = this.chartMaxHeight;

        int maxValue1 = maxValue;
        int minValue1 = minValue;

        int maxValue1Left = 0, minValue1Left = 0, maxValue1Right = 0, minValue1Right = 0;

        if (data.is2YAxis) {
            maxValue1Left = maxValue;
            minValue1Left = minValue;

            maxValue1Right = maxValueRight;
            minValue1Right = minValueRight;
        }
        
        if (changeVisibilityAnimate || changeMotionYAnimate) {
            int sizeBetweenLines1 = lineValuesLast.get(lineValuesLast.size() - 1) - lineValuesLast.get(lineValuesLast.size() - 2);
            maxValue1 = lineValuesLast.get(lineValuesLast.size() - 1) + sizeBetweenLines1;
            minValue1 = lineValuesLast.get(0);

            if (data.is2YAxis && lineValuesRightLast.size() > 0) {
                sizeBetweenLines1 = lineValuesRightLast.get(lineValuesRightLast.size() - 1) - lineValuesRightLast.get(lineValuesRightLast.size() - 2);
                maxValue1Right = lineValuesRightLast.get(lineValuesRightLast.size() - 1) + sizeBetweenLines1;
                minValue1Right = lineValuesRightLast.get(0);
            }

            if (maxValue == 6) {
                
                maxValue = maxValue1;
                minValue = minValue1;
            }

            if (maxValue1 == 6) {
                maxValue1 = maxValue;
                minValue1 = minValue;
            }

            if (maxValueRight == 6) {
                
                maxValueRight = maxValue1Right;
                minValue1Right = minValue1Right;
            }

            maxValueLeft = maxValue;
            minValueLeft = minValue;

            maxValue1Left = maxValue1;
            minValue1Left = minValue1;
        }

        float x1, y1, x2, y2;

        int bPosInFor = (int) (bPos - countElementsForBorders);
        int ePosInFor = (int) (ePos + countElementsForBorders);

        if (bPosInFor < 0) bPosInFor = 0;
        if (ePosInFor > data.countElements) ePosInFor = data.countElements;

        int countElements = data.countElements;
        float bPosF = (scrollAreaX - marginsX) * ((float) countElements - 1) / (width - (marginsX * 2));

        double value = bPosF;
        double fractionalPart = value % 1;

        float a = 1 - (float) fractionalPart;
        float shiftFractal = (float) fractionalPart * xShift;
        shiftFractal = 0;
        float rel1, rel2, rel1Last, rel2Last;

        if (true) {
            if (data.type == CHART_TYPE_LINE) {
                formPath(paths, false);

                if (changeVisibilityAnimate) {
                    formPath(pathsForScrollPanel, true);
                }

                for (int i = 0; i < paths.size(); i++) {
                    if (changeVisibilityAnimate && data.functions.get(i).needChangeVisibility) {
                        functionPaints.get(i).setAlpha(calcAlpha(i));
                        canvas.drawPath(paths.get(i), functionPaints.get(i));
                        functionPaints.get(i).setAlpha(255);
                    } else if (data.functions.get(i).isVisible) {
                        canvas.drawPath(paths.get(i), functionPaints.get(i));
                    }
                }

                Log.d("valueWasChosenOnChart = ", ""+valueWasChosenOnChart);

                if (valueWasChosenOnChart && getCountVisibleElements() > 0) {
                    chosenValueOnChartYMinPos = beginPos;

                    for (int i = bPosInFor; i < ePosInFor; i++) {
                        if (i == chosenValueOnChart) {
                            x1 = marginsX - shiftFractal + xShift * (i - bPos);
                            chosenValueOnChartXPos = x1;

                            if (x1 > marginsX * 1.1f && x1 < width - (marginsX * 1.1f)) {
                                Paint p16 = new Paint();
                                p16.setStrokeWidth(dp(2));
                                p16.setColor(Color.parseColor(rectChosenValueLineColor));
                                canvas.drawLine(x1, dp(50), x1, beginPos, p3);
                            }

                            for (int j = 0; j < data.functions.size(); j++) {
                                if (data.functions.get(j).isVisible) {
                                    if (data.is2YAxis) {
                                        maxValue = j == 0 ? maxValueLeft : maxValueRight;
                                        minValue = j == 0 ? minValueLeft : minValueRight;
                                    }

                                    y1 = (int) (beginPos - (((data.functions.get(j).points.get(i) - minValue) / (float) (maxValue - minValue)) * chartMaxHeight));

                                    if (y1 < chosenValueOnChartYMinPos) chosenValueOnChartYMinPos = y1;

                                    canvas.drawCircle(x1, y1, dp(5), functionPaints.get(j));
                                    Paint p15 = new Paint();
                                    p15.setColor(Color.parseColor(backgroundColor));
                                    canvas.drawCircle(x1, y1, dp(3.5f), p15);
                                }
                            }
                        }
                    }
                }
            }


            if (data.type == CHART_TYPE_STACK) {
                formPath(paths, false);

                if (changeVisibilityAnimate) {
                    formPath(pathsForScrollPanel, true);
                }

                for (int i = 0; i < paths.size(); i++) {
                    if (data.type != CHART_TYPE_STACK && data.type != CHART_TYPE_PERCENTAGE && changeVisibilityAnimate && data.functions.get(i).needChangeVisibility) {
                        functionPaints.get(i).setAlpha(calcAlpha(i));
                        canvas.drawPath(paths.get(i), functionPaints.get(i));
                        functionPaints.get(i).setAlpha(255);
                    } else if (data.functions.get(i).isVisible) {
                        if (valueWasChosenOnChart) {
                            functionPaints.get(i).setAlpha(127);
                            canvas.drawPath(paths.get(i), functionPaints.get(i));
                            functionPaints.get(i).setAlpha(255);
                        } else {
                            canvas.drawPath(paths.get(i), functionPaints.get(i));
                        }
                    }
                }

                if (valueWasChosenOnChart) {
                    float by;
                        
                    int i = chosenValueOnChart + 1;

                    x1 = marginsX - shiftFractal + xShift * (i - bPos - 1);
                    x2 = marginsX - shiftFractal + xShift * (i - bPos);

                    chosenValueOnChartXPos = x1;

                    by = beginPos;

                    for (int j = 0; j < data.functions.size(); j++) {
                        if (data.functions.get(j).isVisible) {
                            y1 = (int) (by - (((data.functions.get(j).points.get(i - 1) - minValue) / (float) (maxValue - minValue)) * chartMaxHeight));
                            canvas.drawRect(x1, by, x2, y1, functionPaints.get(j));
                            by = y1;
                        }
                    }
                }
            }

            if (data.type == CHART_TYPE_PERCENTAGE) {
                formPath(paths, false);

                if (changeVisibilityAnimate) {
                    formPath(pathsForScrollPanel, true);
                }

                for (int i = 0; i < paths.size(); i++) {
                    if (changeVisibilityAnimate && data.functions.get(i).needChangeVisibility) {
                        canvas.drawPath(paths.get(i), functionPaints.get(i));
                    } else if (data.functions.get(i).isVisible) {
                        canvas.drawPath(paths.get(i), functionPaints.get(i));
                    }
                }

                if (valueWasChosenOnChart) {
                    for (int i = bPosInFor; i < ePosInFor; i++) {
                        if (i == chosenValueOnChart) {
                            x1 = marginsX - shiftFractal + xShift * (i - bPos);

                            chosenValueOnChartXPos = x1;

                            canvas.drawLine(x1, dp(50), x1, beginPos, p3);
                            
                        }
                    }
                }
            }
        }
    }

    PointF calcAnim(int maxValue, int minValue, int maxValue1, int minValue1, int funcId, int pointId, float beginPos, float chartMaxHeight) {
        float y1, y2;

        float rel1, rel1Last;
        float rel2 = 0, rel2Last = 0;


        int time = 0;
        int duration = 1;

        if (changeVisibilityAnimate) {
            time = changeVisibilityAnimationTime;
            duration = changeVisibilityAnimationDuration;
        }

        if (changeMotionYAnimate) {
            time = changeMotionYAnimationTime;
            duration = changeMotionYAnimationDuration;
        }

        float percentOfAnimation = (float)time / duration;

        int j = funcId;
        int i = pointId;

        rel1 = (data.functions.get(j).points.get(i - 1) - minValue) / (float)(maxValue - minValue);
        if (i < data.functions.get(j).points.size())
            rel2 = (data.functions.get(j).points.get(i) - minValue) / (float)(maxValue - minValue);

        rel1Last = (data.functions.get(j).points.get(i - 1) - minValue1) / (float)(maxValue1 - minValue1);
        if (i < data.functions.get(j).points.size())
            rel2Last = (data.functions.get(j).points.get(i) - minValue1) / (float)(maxValue1 - minValue1);

        rel1 = (rel1 + ((rel1Last - rel1) * (1 - percentOfAnimation)));
        rel2 = (rel2 + ((rel2Last - rel2) * (1 - percentOfAnimation)));

        y1 = (int) (beginPos - (rel1 * chartMaxHeight));
        y2 = (int) (beginPos - (rel2 * chartMaxHeight));

        if (y1 > beginPos) y1 = beginPos + dp(4);
        if (y2 > beginPos) y2 = beginPos + dp(4);

        if (y1 < beginPos - (chartMaxHeight * 1.5f)) y1 = beginPos - (chartMaxHeight * 1.5f);
        if (y2 < beginPos - (chartMaxHeight * 1.5f)) y2 = beginPos - (chartMaxHeight * 1.5f);

        return new PointF(y1, y2);
    }

    int calcAlpha(int funcId) {
        float percentOfAnimation = changeVisibilityAnimationTime / (float) changeVisibilityAnimationDuration;

        float alpha;
        if (data.functions.get(funcId).isVisible) {
            alpha = percentOfAnimation * 255;
            if (alpha > 255) alpha = 255;
        } else {
            alpha = (1 - percentOfAnimation) * 255;
            if (alpha < 0) alpha = 0;
        }
        return (int)alpha;
    }

    void drawDates(Canvas canvas) {
        int yShift = (int) dp(50);
        int beginPos = this.beginPos + (int)dp(30);
        int textMarginY = (int) dp(5);
        
        float xShift = (float) width / 6;
        float datesShift = (ePos - bPos) / 5;

        Paint p17 = new Paint();
        p17.setColor(Color.parseColor(backgroundColor));

        canvas.drawRect(0, this.beginPos + dp(2), width, this.beginPos + (int)dp(45), p17);
        canvas.drawRect(0, this.beginPos + dp(102), width, this.beginPos + (int)dp(152), p17);

        for (int i = 0; i < 6; i++) {
            int datePos = (int)(bPos + (datesShift * i));
            if (i == 5 && datePos < ((int)ePos)) {
                datePos = (int)ePos;
            }
            if (datePos >= data.dates.size()) datePos = data.dates.size() - 1;

            canvas.drawText(String.valueOf(data.dates.get(datePos)), (xShift * i) + (xShift / 2), beginPos, p6);
        }
    }

    RectF rectChosenValueDialog;

    int getCountVisibleElements() {
        int countVisibleElements = 0;

        for (int i = 0; i < data.functions.size(); i++) {
            if (data.functions.get(i).isVisible) {
                countVisibleElements++;
            }
        }
        return countVisibleElements;
    }

    ArrayList<Integer> percentagesInDialog = new ArrayList<>();

    void drawChosenValueDialogOnChart(Canvas canvas) {
        if (data.isDesignV1) {
            drawChosenValueDialogOnChartV1(canvas);
            return;
        }

        if (!valueWasChosenOnChart) return;
        if (changeVisibilityAnimate || changeMotionYAnimate) return;

        float width = dp(100);
        float height = dp(60);
        float x = dp(250);
        float y = dp(50);

        x = chosenValueOnChartXPos - (width / 3);
        int countVisibleElements = 0;

        for (int i = 0; i < data.functions.size(); i++) {
            if (data.functions.get(i).isVisible) {
                countVisibleElements++;
            }
        }

        if (countVisibleElements > 2) {
            height = dp(92);
        }
        width = dp(160);
        height = dp(26) + (dp(20) * countVisibleElements);

        if (data.type == CHART_TYPE_STACK && countVisibleElements > 1) {
            height = dp(26) + (dp(20) * (countVisibleElements + 1));
        }

        if (countVisibleElements == 0) {
            valueWasChosenOnChart = false;
            return;
        }

        Log.d("chVD", "y + h = "+(y + height)+" "+chosenValueOnChartYMinPos);
        Log.d("chosenValueOnChartXPos", ""+chosenValueOnChartXPos);

        if (chosenValueOnChartXPos > (this.width / 2)) {
            x = chosenValueOnChartXPos - width - dp(25);
        } else {
            x = chosenValueOnChartXPos + dp(25);
        }

        if (x < marginsX * 1.5f) x = marginsX * 1.5f;
        if (x + width > this.width - (marginsX * 1.5f)) x = this.width - (marginsX * 1.5f) - width;

        rectChosenValueDialog = new RectF(x, y, x + width, y + height);

        int visibleElementPos = 0;
        int visibleElementCount = 0;

        float xShifts[] = new float[]{x + dp(10), x + (width / 2) + dp(10), x + dp(10), x + (width / 2) + dp(10)};
        float yShifts[] = new float[]{y + dp(40), y + dp(40), y + dp(72), y + dp(72)};

        float border = dp(0.5f);
        if (border == 0) border = 1;
        float angle = 15;

        Paint p9 = new Paint();
        Paint p10 = new Paint();
        p10.setColor(Color.parseColor(rectChosenValueBackgroundColor));

        border = border * 2;
        p9.setColor(Color.parseColor(rectChosenValueBorder2Color));
        canvas.drawRoundRect(new RectF(x - border, y - border, x + width + border, y + height + border), angle, angle, p9);
        border = border / 2;
        p9.setColor(Color.parseColor(rectChosenValueBorder1Color));
        canvas.drawRoundRect(new RectF(x - border, y - border, x + width + border, y + height + border), angle, angle, p9);
        canvas.drawRoundRect(new RectF(x, y, x + width, y + height), angle, angle, p10);

        Paint p11 = new Paint();
        p11.setAntiAlias(true);
        p11.setTextSize(dp(13));
        p11.setTypeface(Typeface.DEFAULT_BOLD);
        p11.setColor(Color.parseColor(rectChosenValueDateColor));

        if (data.datesWithNameOfDayAndYear.size() > chosenValueOnChart)
            canvas.drawText(data.datesWithNameOfDayAndYear.get(chosenValueOnChart), x + dp(10), y + dp(18), p11);

        if (percentagesInDialog.size() == 0) {
            for (int i = 0; i < data.functions.size(); i++) {
                percentagesInDialog.add(0);
            }
        }

        for (int i = 0; i < data.functions.size(); i++) {
            if (data.functions.get(i).isVisible) {
                visibleElementCount += data.functions.get(i).points.get(chosenValueOnChart);
            }
        }

        int maxPercentage = 0;
        int maxPercentagePos = 0;
        int percentageSum = 0;

        if (data.type == CHART_TYPE_PERCENTAGE) {
            for (int i = 0; i < data.functions.size(); i++) {
                if (data.functions.get(i).isVisible) {
                    
                    int percentage = Math.round(data.functions.get(i).points.get(chosenValueOnChart) * 100 / (float) visibleElementCount);
                    percentagesInDialog.set(i, percentage);
                    if (maxPercentage < percentage) {
                        maxPercentage = percentage;
                        maxPercentagePos = i;
                    }
                    percentageSum += percentage;
                }
            }

            Log.d("percentageSum ", "" + percentageSum + " " + maxPercentage + " " + data.functions.get(maxPercentagePos).name + " " + percentagesInDialog.get(maxPercentagePos) + " " + maxPercentagePos);
            if (percentageSum < 100) {
                
                percentagesInDialog.set(maxPercentagePos, percentagesInDialog.get(maxPercentagePos) + (100 - percentageSum));
            }

            if (percentageSum > 100) {
                
                percentagesInDialog.set(maxPercentagePos, percentagesInDialog.get(maxPercentagePos) - (percentageSum - 100));
            }
        }

        for (int i = 0; i < data.functions.size(); i++) {
            if (data.functions.get(i).isVisible) {
                p11.setColor(Color.parseColor(data.functions.get(i).color));
                p11.setTextSize(dp(13));
                p11.setTypeface(Typeface.DEFAULT_BOLD);
                p11.setTextAlign(Paint.Align.RIGHT);
                canvas.drawText(formatNumberIfNeedV2(data.functions.get(i).points.get(chosenValueOnChart)), x + width - dp(10), y + dp(20) + (dp(20) * visibleElementPos) + dp(18), p11);

                float percentageShift = 0;

                if (data.type == CHART_TYPE_PERCENTAGE) {
                    p11.setColor(Color.parseColor(rectChosenValueDateColor));
                    p11.setTextSize(dp(13));
                    p11.setTypeface(Typeface.DEFAULT_BOLD);
                    p11.setTextAlign(Paint.Align.RIGHT);

                    float addShift = countVisibleElements == 1 ? dp(8) : 0;
                    canvas.drawText(percentagesInDialog.get(i)+"%", x + dp(33) + addShift, y + dp(20) + (dp(20) * visibleElementPos) + dp(18), p11);
                    Log.d("percInD", ""+i+" "+percentagesInDialog.get(i));
                    percentageShift = dp(32) + addShift;
                }

                p11.setColor(Color.parseColor(rectChosenValueDateColor));
                p11.setTextSize(dp(13));
                
                p11.setTypeface(Typeface.DEFAULT);
                p11.setTextAlign(Paint.Align.LEFT);
                canvas.drawText(data.functions.get(i).name, x + dp(10) + percentageShift, y + dp(20) + (dp(20) * visibleElementPos) + dp(18), p11);

                visibleElementPos++;
            }
        }

        if (data.type == CHART_TYPE_STACK && countVisibleElements > 1) {
            p11.setColor(Color.parseColor(rectChosenValueDateColor));
            p11.setTextSize(dp(13));
            p11.setTypeface(Typeface.DEFAULT_BOLD);
            p11.setTextAlign(Paint.Align.RIGHT);
            canvas.drawText(formatNumberIfNeedV2(visibleElementCount), x + width - dp(10), y + dp(20) + (dp(20) * visibleElementPos) + dp(18), p11);

            p11.setColor(Color.parseColor(rectChosenValueDateColor));
            p11.setTextSize(dp(13));
            
            p11.setTypeface(Typeface.DEFAULT);
            p11.setTextAlign(Paint.Align.LEFT);
            canvas.drawText("All", x + dp(10), y + dp(20) + (dp(20) * visibleElementPos) + dp(18), p11);
        }
    }

    void drawChosenValueDialogOnChartV1(Canvas canvas) {
        if (!valueWasChosenOnChart) return;
        if (changeVisibilityAnimate || changeMotionYAnimate) return;

        float width = dp(100);
        float height = dp(60);

        float x = dp(250);
        float y = dp(50);

        x = chosenValueOnChartXPos - (width / 3);

        if (chosenValueOnChartXPos > (this.width / 2)) {
            x = chosenValueOnChartXPos - width - dp(25);
        } else {
            x = chosenValueOnChartXPos + dp(25);
        }

        if (x < marginsX * 1.5f) x = marginsX * 1.5f;
        if (x + width > this.width - (marginsX * 1.5f)) x = this.width - (marginsX * 1.5f) - width;


        int countVisibleElements = 0;

        for (int i = 0; i < data.functions.size(); i++) {
            if (data.functions.get(i).isVisible) {
                countVisibleElements++;
            }
        }

        if (countVisibleElements > 2) {
            height = dp(92);
        }

        if (countVisibleElements == 0) {
            return;
        }

        rectChosenValueDialog = new RectF(x, y, x + width, y + height);

        int visibleElementPos = 0;

        float xShifts[] = new float[]{x + dp(10), x + (width / 2) + dp(10), x + dp(10), x + (width / 2) + dp(10)};
        float yShifts[] = new float[]{y + dp(40), y + dp(40), y + dp(72), y + dp(72)};


        float border = dp(0.5f);
        if (border == 0) border = 1;

        float angle = 15;

        Paint p9 = new Paint();
        Paint p10 = new Paint();
        p10.setColor(Color.parseColor(rectChosenValueBackgroundColor));

        border = border * 2;
        p9.setColor(Color.parseColor(rectChosenValueBorder2Color));
        canvas.drawRoundRect(new RectF(x - border, y - border, x + width + border, y + height + border), angle, angle, p9);
        border = border / 2;
        p9.setColor(Color.parseColor(rectChosenValueBorder1Color));
        canvas.drawRoundRect(new RectF(x - border, y - border, x + width + border, y + height + border), angle, angle, p9);
        canvas.drawRoundRect(new RectF(x, y, x + width, y + height), angle, angle, p10);

        Paint p11 = new Paint();
        p11.setAntiAlias(true);
        p11.setTextSize(dp(12));
        p11.setColor(Color.parseColor(rectChosenValueDateColor));
        canvas.drawText(data.datesWithNameOfDay.get(chosenValueOnChart), x + dp(10), y + dp(18), p11);

        for (int i = 0; i < data.functions.size(); i++) {
            if (data.functions.get(i).isVisible) {
                p11.setColor(Color.parseColor(data.functions.get(i).color));
                p11.setTextSize(dp(14));
                canvas.drawText(formatNumberIfNeed(data.functions.get(i).points.get(chosenValueOnChart)), xShifts[visibleElementPos], yShifts[visibleElementPos], p11);
                p11.setTextSize(dp(12));
                canvas.drawText(data.functions.get(i).name, xShifts[visibleElementPos], yShifts[visibleElementPos] + dp(14), p11);
                visibleElementPos++;
            }
        }
    }

    void drawScrollPanel(Canvas canvas) {
        Paint p7 = new Paint();
        p7.setColor(Color.parseColor(scrollPanelColor));
        canvas.drawRect(marginsX, scrollAreaY, width - marginsX, scrollAreaY + scrollAreaHeight, p7);

        if (!data.isDesignV1) {
            p7.setColor(Color.parseColor(backgroundColor));
            canvas.drawRect(scrollAreaX, scrollAreaY, scrollAreaX + scrollAreaWidth, scrollAreaY + scrollAreaHeight, p7);
        }

        if (data.type == CHART_TYPE_STACK || data.type == CHART_TYPE_PERCENTAGE) {
            
            drawChartInScrollPanel(canvas);
        }

        Paint p8 = new Paint();
        p8.setColor(Color.parseColor(scrollPanelBordersColor));

        Paint p10 = new Paint();
        p10.setColor(Color.WHITE);

        if (data.isDesignV1) {
            canvas.drawRect(scrollAreaX - dp(2), scrollAreaY, scrollAreaX + scrollAreaWidth + dp(2), scrollAreaY + scrollAreaHeight, p8);
            canvas.drawRect(scrollAreaX, scrollAreaY + dp(1), scrollAreaX + scrollAreaWidth, scrollAreaY + scrollAreaHeight - dp(1), p4);
        } else {
            p8.setColor(Color.parseColor(isDarkTheme ? "#304259" : "#86A9C4"));
            p8.setAlpha(isDarkTheme ? 153 : 127);
            
            canvas.drawRect(scrollAreaX, scrollAreaY, scrollAreaX + dp(8), scrollAreaY + scrollAreaHeight, p8);
            canvas.drawRect(scrollAreaX + scrollAreaWidth - dp(8), scrollAreaY, scrollAreaX + scrollAreaWidth, scrollAreaY + scrollAreaHeight, p8);

            canvas.drawRect(scrollAreaX + dp(3), scrollAreaY + (scrollAreaHeight / 2) - (scrollAreaHeight / 6), scrollAreaX + dp(5), scrollAreaY + (scrollAreaHeight / 2) + (scrollAreaHeight / 6), p10);
            canvas.drawRect(scrollAreaX + scrollAreaWidth - dp(5), scrollAreaY + (scrollAreaHeight / 2) - (scrollAreaHeight / 6), scrollAreaX + scrollAreaWidth - dp(3), scrollAreaY + (scrollAreaHeight / 2) + (scrollAreaHeight / 6), p10);

            canvas.drawRect(scrollAreaX, scrollAreaY - dp(1), scrollAreaX + scrollAreaWidth, scrollAreaY, p8);
            canvas.drawRect(scrollAreaX, scrollAreaY + scrollAreaHeight, scrollAreaX + scrollAreaWidth, scrollAreaY + scrollAreaHeight + dp(1), p8);
        }

        if (data.type == CHART_TYPE_LINE) {
            drawChartInScrollPanel(canvas);
        }
    }

    void drawChartInScrollPanelByPath(Canvas canvas) {
        for (int i = 0; i < pathsForScrollPanel.size(); i++) {
            if (data.type == CHART_TYPE_LINE && changeVisibilityAnimate && data.functions.get(i).needChangeVisibility) {
                functionPaintsForScrollPanel.get(i).setAlpha(calcAlpha(i));
                canvas.drawPath(pathsForScrollPanel.get(i), functionPaintsForScrollPanel.get(i));
                functionPaintsForScrollPanel.get(i).setAlpha(255);
            } else if (data.functions.get(i).isVisible) {
                canvas.drawPath(pathsForScrollPanel.get(i), functionPaintsForScrollPanel.get(i));
            } else if (data.type != CHART_TYPE_LINE && data.functions.get(i).needChangeVisibility) {
                canvas.drawPath(pathsForScrollPanel.get(i), functionPaintsForScrollPanel.get(i));
            }
        }
    }

    boolean pathWasFormForScrollPanel;
    
    ArrayList<Path> pathsForScrollPanel = new ArrayList<>();

    void drawChartInScrollPanel(Canvas canvas) {
        if (true) {
            drawChartInScrollPanelByPath(canvas);
            return;
        }
        
        float xShift = (width - (marginsX * 2)) / ((float)data.countElements - 1);
        int beginPos = this.beginPos + (int)dp(100);

        int maxValue = getMaxValueForBuildChart(getMaxValueByVisibleFunctions());
        int minValue = getMinValueForBuildChart(getMinValueByVisibleFunctions(), maxValue);

        int maxValue1 = 0;
        int minValue1 = 0;

        if (changeVisibilityAnimate || changeMotionYAnimate) {
            maxValue1 = scrollPanelAnimationMaxValueBegin;
            minValue1 = scrollPanelAnimationMinValueBegin;

            maxValue = scrollPanelAnimationMaxValueEnd;
            minValue = scrollPanelAnimationMinValueEnd;
        }

        maxValue1 = minValue1 + ((maxValue1 - minValue1) * 6 / 5);
        maxValue = minValue + ((maxValue - minValue) * 6 / 5);

        int chartMaxHeight = (int)dp(50);
        float x1, y1, x2, y2;
        float percentOfAnimation = 0;
        float rel1, rel2, rel1Last, rel2Last;

        if (changeVisibilityAnimate) {
            percentOfAnimation = changeVisibilityAnimationTime / (float) changeVisibilityAnimationDuration;
        }

        if (changeMotionYAnimate) {
            percentOfAnimation = changeMotionYAnimationTime / (float) changeMotionYAnimationDuration;
        }
        
        if (changeVisibilityAnimate) {
            if (maxValue == 5 || maxValue == 6) {
                maxValue = maxValue1 / 2;
            }

            for (int i = 1; i < data.countElements; i++) {
                x1 = marginsX + xShift * (i - 1);
                x2 = marginsX + xShift * i;

                for (int j = 0; j < data.functions.size(); j++) {
                    if (data.functions.get(j).isVisible || data.functions.get(j).needChangeVisibility) {
                        rel1 = (data.functions.get(j).points.get(i - 1) - minValue) / (float) (maxValue - minValue);
                        rel2 = (data.functions.get(j).points.get(i) - minValue) / (float) (maxValue - minValue);

                        rel1Last = (data.functions.get(j).points.get(i - 1) - minValue1) / (float) (maxValue1 - minValue1);
                        rel2Last = (data.functions.get(j).points.get(i) - minValue1) / (float) (maxValue1 - minValue1);

                        y1 = 0;
                        y2 = 0;

                        rel1 = (rel1 + ((rel1Last - rel1) * (1 - percentOfAnimation)));
                        rel2 = (rel2 + ((rel2Last - rel2) * (1 - percentOfAnimation)));

                        y1 = (int) (beginPos - (rel1 * chartMaxHeight));
                        y2 = (int) (beginPos - (rel2 * chartMaxHeight));

                        if (y1 > beginPos) y1 = beginPos + dp(4);
                        if (y2 > beginPos) y2 = beginPos + dp(4);

                        if (y1 < beginPos - chartMaxHeight) y1 = beginPos - chartMaxHeight - dp(4);
                        if (y2 < beginPos - chartMaxHeight) y2 = beginPos - chartMaxHeight - dp(4);

                        if (data.functions.get(j).needChangeVisibility) {

                            float alpha;

                            if (data.functions.get(j).isVisible) {
                                alpha = percentOfAnimation * 255;
                                if (alpha > 255) alpha = 255;
                            } else {
                                alpha = (1 - percentOfAnimation) * 255;
                                if (alpha < 0) alpha = 0;
                            }

                            functionPaints.get(j).setAlpha((int)alpha);
                            

                            canvas.drawLine(x1, y1, x2, y2, functionPaints.get(j));
                            functionPaints.get(j).setAlpha(255);
                        } else {
                            
                            canvas.drawLine(x1, y1, x2, y2, functionPaints.get(j));
                        }
                    }
                }
            }
        } else {
            for (int i = 1; i < data.countElements; i++) {
                x1 = marginsX + xShift * (i - 1);
                x2 = marginsX + xShift * i;

                for (int j = 0; j < data.functions.size(); j++) {
                    if (data.functions.get(j).isVisible) {
                        y1 = (int) (beginPos - (((data.functions.get(j).points.get(i - 1) - minValue) / (float)(maxValue - minValue)) * chartMaxHeight));
                        y2 = (int) (beginPos - (((data.functions.get(j).points.get(i) - minValue) / (float)(maxValue - minValue)) * chartMaxHeight));
                        canvas.drawLine(x1, y1, x2, y2, functionPaints.get(j));
                    }
                }
            }
        }
    }

    float dp(float px) {
        Resources r = getResources();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, px, r.getDisplayMetrics());
    }

    String formatNumberIfNeed(int a) {
        if (a > 1000000) {
            float val = (float)a / 1000000;
            return String.format(Locale.US, "%.1f", val)+"M";
        }
        if (a > 100000) {
            float val = (float)a / 1000;
            return String.format(Locale.US, "%.0f", val)+"K";
        }
        if (a > 1000) {
            float val = (float)a / 1000;
            return String.format(Locale.US, "%.1f", val)+"K";
        }
        return String.valueOf(a);
    }

    String formatNumberIfNeedV2(int a) {
        if (a > 1000000) {
            String b = String.valueOf((a % 1000000) / 1000);
            if (b.length() == 1) b = "00" + b;
            if (b.length() == 2) b = "0" + b;

            String c = String.valueOf((a % 1000));
            if (c.length() == 1) c = "00" + c;
            if (c.length() == 2) c = "0" + c;
            return String.valueOf((a / 1000000))+" "+b+" "+c;
        }
        if (a > 1000) {
            String c = String.valueOf((a % 1000));
            if (c.length() == 1) c = "00" + c;
            if (c.length() == 2) c = "0" + c;

            return String.valueOf((a / 1000))+" "+c;
        }
        return String.valueOf(a);
    }

}
