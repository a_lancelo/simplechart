package com.gram.chart;

import android.content.Context;
import android.content.res.AssetManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Andrew on 13.04.2019.
 */

public class AppData {

    static AppData appData;
    static Context context;

    static AppData getInstance() {
        if (appData == null) {
            appData = new AppData();
        }
        return appData;
    }

    static int loadDataStatus = -1;

    static ArrayList<ChartData> allTestData = new ArrayList<>();

    static void loadAllTestData() {
        ArrayList<ChartData> testData = readTestDataFromFile();
        for (int i = 0; i < 5; i++) {
            allTestData.add(testData.get(i));
        }

        allTestData.add(getMyTestData());

        testData = readTestDataFromFileV2();
        for (int i = 0; i < 5; i++) {
            allTestData.add(testData.get(i));
        }

        for (int i = 0; i < 6; i++) {
            allTestData.get(i).isDesignV1 = true;
        }

        allTestData.get(6).isSupportZoom = true;

        /*for (int i = 6; i < 11; i++) {
            allTestData.get(i).isSupportZoom = true;
        }*/

        //0 - line
        //2 - stacked
        //3 - percentage

        allTestData.get(6).type = 0;
        allTestData.get(7).type = 0;
        allTestData.get(8).type = 2;
        allTestData.get(9).type = 2;
        allTestData.get(10).type = 3;

        allTestData.get(7).is2YAxis = true;

        loadDataStatus = 0;

        if (true) return;

        //for (int i = 0; i < 4; i++) {
        for (int i = 0; i < 1; i++) {
            allTestData.get(i + 6).allDataZoomed = readTestZoomedDataFromFileV2(allTestData.get(i + 6), (i + 1));
            loadDataStatus = (i + 1);
        }
    }

    static void resetAllTestDataStates() {
        for (int i = 0; i < allTestData.size(); i++) {
            for (int j = 0; j < allTestData.get(i).functions.size(); j++) {
                allTestData.get(i).functions.get(j).isVisible = true;
                allTestData.get(i).functions.get(j).needChangeVisibility = false;
                allTestData.get(i).functions.get(j).isVisibleNewState = false;
            }
        }
    }

    static ArrayList<ChartData> readTestDataFromFile() {
        ArrayList<ChartData> dataList = new ArrayList<>();
        //String jsonString = readFileFromResources(R.raw.chart_data);
        String path = "chart_data.json";
        String jsonString = readFileFromAssets(path);
        try {
            JSONArray array = new JSONArray(jsonString);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                ChartData data = formChartDataByJSONObject(object);
                dataList.add(data);
            }
        } catch (Exception e) {}
        return dataList;
    }

    static ArrayList<ChartData> readTestDataFromFileV2() {
        ArrayList<ChartData> dataList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            String path = "contest/" + String.valueOf(i + 1) + "/overview.json";
            dataList.add(readTestDataFromFileV2(path));
        }
        return dataList;
    }

    static ChartData readTestDataFromFileV2(String path) {
        String jsonString = readFileFromAssets(path);
        return formChartDataByJSONString(jsonString);
    }

    static ChartData formChartDataByJSONString(String jsonString) {
        ChartData data = new ChartData();
        try {
            data = formChartDataByJSONObject(new JSONObject(jsonString));
        } catch (Exception e) {}
        return  data;
    }

    static ChartData formChartDataByJSONObject(JSONObject object) {
        ChartData data = new ChartData();

        try {
            JSONArray columns = object.getJSONArray("columns");

            for (int j = 0; j < columns.length(); j++) {
                JSONArray columnArray = columns.getJSONArray(j);
                if (columnArray.length() > 0) {
                    ChartFunction func = new ChartFunction();
                    String columnName = columnArray.getString(0);
                    boolean isColumnX = columnName.contains("x");

                    for (int k = 1; k < columnArray.length(); k++) {
                        if (isColumnX) {
                            data.x.add(columnArray.getLong(k));
                        } else {
                            func.points.add(columnArray.getInt(k));
                        }
                    }

                    if (isColumnX) {
                        data.countElements = data.x.size();
                    } else {
                        func.columnName = columnName;
                        func.isVisible = true;
                        data.functions.add(func);
                    }
                }
            }

            JSONObject types = object.getJSONObject("types");
            for (int j = 0; j < data.functions.size(); j++) {
                data.functions.get(j).type = types.getString(data.functions.get(j).columnName);
            }

            JSONObject names = object.getJSONObject("names");
            JSONObject colors = object.getJSONObject("colors");
            for (int j = 0; j < data.functions.size(); j++) {
                data.functions.get(j).name = names.getString(data.functions.get(j).columnName);
                data.functions.get(j).color = colors.getString(data.functions.get(j).columnName);
            }

            formDateFromUnicode(data);
        } catch (Exception e) {

        }

        return data;
    }

    static ArrayList<ChartData> readTestZoomedDataFromFileV2(ChartData data, int chartId) {
        ArrayList<ChartData> dataList = new ArrayList<>();
        for (int i = 0; i < data.dates.size(); i++) {
            String path = "contest/"+String.valueOf(chartId)+"/"+formDateForPath(data.x.get(i))+".json";
            dataList.add(readTestDataFromFileV2(path));
        }
        return dataList;
    }

    static String formDateForPath(Long date) {
        Date currentDate = new Date(date);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM/dd", Locale.UK);
        String dateText = dateFormat.format(currentDate);
        return dateText;
    }

    static String readFileFromAssets(String filePath) {
        try {
            AssetManager am = context.getAssets();
            InputStream is = am.open(filePath);
            Writer writer = new StringWriter();
            char[] buffer = new char[1024];

            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
            is.close();

            return writer.toString();
        } catch (Exception e) {}

        return "";
    }

    static String readFileFromResources(int id) {
        InputStream is = context.getResources().openRawResource(id);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
            is.close();
        } catch (Exception e) {}

        return writer.toString();
    }

    static void formDateFromUnicode(ChartData data) {
        for (int i = 0; i < data.x.size(); i++) {
            Date currentDate = new Date(data.x.get(i));
            DateFormat dateFormat = new SimpleDateFormat("MMM dd", Locale.UK);
            String dateText = dateFormat.format(currentDate);
            data.dates.add(dateText);

            dateFormat = new SimpleDateFormat("EEE, MMM dd", Locale.UK);
            dateText = dateFormat.format(currentDate);
            data.datesWithNameOfDay.add(dateText);

            dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy", Locale.UK);
            dateText = dateFormat.format(currentDate);
            data.datesWithNameOfDayAndYear.add(dateText);

            dateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.UK);
            dateText = dateFormat.format(currentDate);
            data.datesWithYear.add(dateText);
        }
    }

    static ChartData getMyTestData() {
        ChartData data = new ChartData();
        data.countElements = 10;

        int a[] = new int[]{100, 125, 85, 220, 150, 185, 110, 160, 50, 100};
        int b[] = new int[]{50, 75, 85, 100, 80, 90, 102, 65, 30, 50};
        int c[] = new int[]{41, 80, 85, 170, 180, 200, 150, 280, 50, 30};

        ChartFunction func = new ChartFunction();
        func.columnName = "y0";
        for (int i = 0; i < a.length; i++) func.points.add(a[i]);
        func.name = "#0";
        func.type = "line";
        func.color = "#3cc23f";
        func.isVisible = true;
        data.functions.add(func);

        func = new ChartFunction();
        func.columnName = "y1";
        for (int i = 0; i < b.length; i++) func.points.add(b[i]);
        func.name = "#1";
        func.type = "line";
        func.color = "#ed685f";
        func.isVisible = true;
        data.functions.add(func);

        func = new ChartFunction();
        func.columnName = "y2";
        for (int i = 0; i < c.length; i++) func.points.add(c[i]);
        func.name = "#1";
        func.type = "line";
        func.color = "#3896d4";
        func.isVisible = true;
        data.functions.add(func);

        String dates[] = new String[]{"Dec 15", "Dec 31", "Jan 10", "Jan 16", "Jan 31", "Feb 8", "Feb 18", "Feb 23", "Mar 3", "Mar 12"};
        String datesWithNamesOfDay[] = new String[]{"Sat, Dec 15", "Fri, Dec 31", "Mon, Jan 10", "Sat, Jan 16", "Fri, Jan 31",
                "Tue, Feb 8", "Fri, Feb 18", "Tue, Feb 23", "Fri, Mar 3", "Tue, Mar 12"};

        for (int i = 0; i < dates.length; i++) data.dates.add(dates[i]);
        for (int i = 0; i < dates.length; i++) data.datesWithNameOfDay.add(datesWithNamesOfDay[i]);

        return data;
    }

}
