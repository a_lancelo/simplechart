package com.gram.chart;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Toast;

public class StartScreenActivity extends Activity {

    boolean isStandardTypeAnimation = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppData.context = this;
        new LoadDataTask().execute();

        String namesCharts[] = new String[]{"Stage 1", "Stage 2"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, namesCharts);

        setContentView(R.layout.main);

        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioButton1:
                        isStandardTypeAnimation = true;
                        break;
                    case R.id.radioButton2:
                        isStandardTypeAnimation = false;
                        break;
                }
            }
        });
        radioGroup.setVisibility(View.GONE);

        ListView listView = findViewById(R.id.listView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (AppData.loadDataStatus == -1) {
                    Toast.makeText(StartScreenActivity.this, getString(R.string.toast_1), Toast.LENGTH_SHORT).show();
                    return;
                }
                AppData.resetAllTestDataStates();
                Intent intent = new Intent(StartScreenActivity.this, MainActivity.class);
                intent.putExtra("id", i);
                intent.putExtra("isStandardTypeAnimation", isStandardTypeAnimation);
                startActivity(intent);
            }
        });
    }

    class LoadDataTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            AppData.loadAllTestData();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    }

}
